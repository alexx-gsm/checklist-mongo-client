import axios from 'axios'
import jwt_decode from 'jwt-decode'
import store from '../redux/store'
import { AUTH_SET_USER, logoutUser } from '../redux/modules/auth'

export const setAuthToken = token => {
  if (token) {
    // Applay to every request
    axios.defaults.headers.common['Authorization'] = token
  } else {
    // Delete auth header
    delete axios.defaults.headers.common['Authorization']
  }
}

export const checkAuthToken = () => {
  // Check for token
  if (localStorage.jwtToken) {
    setAuthToken(localStorage.jwtToken)
    const decoded = jwt_decode(localStorage.jwtToken)
    store.dispatch({
      type: AUTH_SET_USER,
      payload: decoded
    })

    // Check for expired token
    const currentTime = Date.now() / 1000
    if (decoded.exp < currentTime) {
      // Logout user
      store.dispatch(logoutUser())

      window.location.href = '/login'
    }
  }
}
