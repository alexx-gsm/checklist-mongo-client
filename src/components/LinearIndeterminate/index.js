import { withStyles } from '@material-ui/core/styles'
import styles from './styles'

import LinearIndeterminate from './LinearIndeterminate'

export default withStyles(styles, { withTheme: true })(LinearIndeterminate)
