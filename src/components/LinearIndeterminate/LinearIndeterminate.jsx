import React from 'react'
import PropTypes from 'prop-types'
import LinearProgress from '@material-ui/core/LinearProgress'

const LinearIndeterminate = ({ classes }) => {
  return (
    <div className={classes.root}>
      <LinearProgress />
      <br />
    </div>
  )
}

LinearIndeterminate.propTypes = {
  classes: PropTypes.object.isRequired
}

export default LinearIndeterminate
