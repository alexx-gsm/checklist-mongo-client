import { withStyles } from '@material-ui/core/styles';
import Sidebar from './Sidebar';
// styles
import styles from './style';

export default withStyles(styles, { withTheme: true })(Sidebar);
