// @material-ui
import { withStyles } from '@material-ui/core/styles'
// styles
import styles from './styles'
// Component
import TopPanel from './TopPanel'

export default withStyles(styles, { withTheme: true })(TopPanel)
