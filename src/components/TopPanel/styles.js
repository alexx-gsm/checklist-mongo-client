export default theme => ({
  PanelTitle: {
    color: theme.palette.grey[600],
    fontWeight: theme.typography.fontWeightLight,
    lineHeight: 1.2,
    // padding: `${theme.spacing.unit * 2}px 0`,

    [theme.breakpoints.down('sm')]: {
      textAlign: 'center',
      padding: '0 5px'
    },
    [theme.breakpoints.down('xs')]: {
      fontSize: '2rem',
      paddingTop: '5px'
    }
  }
})
