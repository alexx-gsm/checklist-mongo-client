import React from 'react'
import classNames from 'classnames'
import { Link } from 'react-router-dom'
import { string, func, bool, object } from 'prop-types'
// material-ui components
import { Grid, TextField, InputAdornment } from '@material-ui/core'
import IconButton from '@material-ui/core/IconButton'
import CircularProgress from '@material-ui/core/CircularProgress'
import Typography from '@material-ui/core/Typography'
// import Fab from '@material-ui/core/Fab'
// @material-ui/icons
import AddIcon from '@material-ui/icons/Add'
import ArrowBack from '@material-ui/icons/ArrowBack'
import Autorenew from '@material-ui/icons/Autorenew'
import IconSearch from '@material-ui/icons/Search'
import IconRemove from '@material-ui/icons/RemoveCircleOutline'
// react icon kit
import { Icon } from 'react-icons-kit'
import { download2 } from 'react-icons-kit/icomoon/download2'
// useful tools
import isEmpty from '../../helpers/is-empty'

const TopPanel = ({
  link,
  title,
  label,
  returnButton,
  upload,
  onReload,
  loading,
  color,
  search,
  classes
}) => {
  return (
    <Grid container className={classes.tools}>
      <Grid item xs={12} sm={6} className={classes.gridTools}>
        {title && (
          <Typography variant='h3' className={classes.PanelTitle}>
            {title}
          </Typography>
        )}
      </Grid>
      <Grid item xs={12} sm={6} className={classes.gridTools} />
    </Grid>
  )
}

TopPanel.defaultProps = {
  link: '',
  title: '',
  upload: false,
  color: 'default',
  returnButton: false,
  loading: false
}

TopPanel.propTypes = {
  link: string,
  title: string,
  returnButton: bool,
  onReload: func,
  loading: bool.isRequired,
  classes: object.isRequired
}

export default TopPanel
