import React from 'react'
import Downshift from 'downshift'
import { TextField, MenuItem, Paper } from '@material-ui/core'

function renderInput(inputProps) {
  const { InputProps, classes, ref, ...other } = inputProps

  return (
    <TextField
      InputProps={{
        inputRef: ref,
        classes: {
          root: classes.inputRoot,
          input: classes.inputInput
        },
        ...InputProps
      }}
      {...other}
    />
  )
}

function renderSuggestion({
  item,
  field,
  index,
  itemProps,
  highlightedIndex,
  selectedItem
}) {
  const isHighlighted = highlightedIndex === index
  const isSelected = (selectedItem || '').indexOf(item[field]) > -1

  return (
    <MenuItem
      {...itemProps}
      key={item.code}
      selected={isHighlighted}
      component='div'
      style={{
        fontWeight: isSelected ? 500 : 400
      }}
    >
      {item[field]}
    </MenuItem>
  )
}

function getSuggestions(value, items, field) {
  const inputValue = value.trim().toLowerCase()
  const inputLength = inputValue.length

  return inputLength > 0
    ? Object.keys(items).reduce((acc, itemId) => {
        return items[itemId][field].slice(0, inputLength).toLowerCase() ===
          inputValue
          ? {
              ...acc,
              [itemId]: items[itemId]
            }
          : acc
      }, {})
    : {}
}

const Autocomplete = ({ items, field, placeholder, onChange, classes }) => {
  return (
    <Downshift
      onChange={item => onChange(item)}
      itemToString={item => (item ? item[field] : '')}
    >
      {({
        getInputProps,
        getItemProps,
        getMenuProps,
        highlightedIndex,
        inputValue,
        isOpen,
        selectedItem
      }) => (
        <div className={classes.container}>
          {renderInput({
            fullWidth: true,
            classes,
            InputProps: getInputProps({
              placeholder
            })
          })}
          <div {...getMenuProps()}>
            {isOpen ? (
              <Paper className={classes.paper} square>
                {Object.keys(getSuggestions(inputValue, items, field)).map(
                  (itemId, index) =>
                    renderSuggestion({
                      item: items[itemId],
                      field,
                      index,
                      itemProps: getItemProps({ item: items[itemId] }),
                      highlightedIndex,
                      selectedItem
                    })
                )}
              </Paper>
            ) : null}
          </div>
        </div>
      )}
    </Downshift>
  )
}

export default Autocomplete
