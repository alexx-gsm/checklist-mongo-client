const ITEM_HEIGHT = 48

export default theme => ({
  root: {
    flexGrow: 1,
    height: 250,
    zIndex: 5
  },
  container: {
    flexGrow: 1,
    position: 'relative'
  },
  paper: {
    position: 'absolute',
    zIndex: 5,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0,
    maxHeight: ITEM_HEIGHT * 5,
    width: 'auto',
    overflow: 'auto'
  },
  chip: {
    margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`
  },
  inputRoot: {
    flexWrap: 'wrap'
  },
  inputInput: {
    width: 'auto',
    flexGrow: 1
  },
  divider: {
    height: theme.spacing.unit * 2
  }
})
