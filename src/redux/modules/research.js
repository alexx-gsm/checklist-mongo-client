import axios from "axios";
import { Record } from "immutable";
import { appName, HOST } from "../../config";

// --- MODULE NAME ---
export const moduleName = "researchStore";

// --- ACTIONS ---
export const RESEARCH_LOADING = `${appName}/${moduleName}/RESEARCH_LOADING`;
export const RESEARCH_GET_ALL_BY_USER = `${appName}/${moduleName}/RESEARCH_GET_ALL_BY_USER`;
export const RESEARCH_GET_ONE = `${appName}/${moduleName}/RESEARCH_GET_ONE`;
export const RESEARCH_STORE = `${appName}/${moduleName}/RESEARCH_STORE`;
export const RESEARCH_SAVE = `${appName}/${moduleName}/RESEARCH_SAVE`;
export const RESEARCH_GET_ALL = `${appName}/${moduleName}/RESEARCH_GET_ALL`;
export const RESEARCH_ERROR = `${appName}/${moduleName}/RESEARCH_ERROR`;

// --- INITIAL STATE ---
const ReducerRecord = Record({
  researches: [],
  research: {},
  error: {},
  loading: false
});

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), { type, payload, error }) => {
  switch (type) {
    case RESEARCH_LOADING:
      return state.set("loading", true);
    case RESEARCH_GET_ALL:
      return state
        .set("loading", false)
        .set("researches", payload)
        .set("error", {});
    case RESEARCH_GET_ONE:
      return state
        .set("loading", false)
        .set("research", payload)
        .set("error", {});
    case RESEARCH_STORE:
      return state
        .set("loading", false)
        .set("research", payload)
        .set("error", {});
    case RESEARCH_SAVE:
      return state
        .set("loading", false)
        .set("research", payload)
        .set("error", {});
    case RESEARCH_ERROR:
      return state.set("loading", false).set("error", error);
    default:
      return state;
  }
};

//! --- AC ---

/**********************
 * * GET RESEARCHES
 **********************/
export const getResearches = () => dispatch => {
  dispatch({
    type: RESEARCH_LOADING
  });
  axios
    .post(`${HOST}/api/researches/user/all`)
    .then(res => {
      dispatch({
        type: RESEARCH_GET_ALL,
        payload: res.data
      });
    })
    .catch(error => {
      dispatch({
        type: RESEARCH_ERROR,
        error: error
      });
    });
};

/**********************
 * * GET RESEARCH
 **********************/
export const getResearch = id => dispatch => {
  dispatch({
    type: RESEARCH_LOADING
  });
  axios
    .post(`${HOST}/api/researches/user/${id}`)
    .then(res => {
      dispatch({
        type: RESEARCH_GET_ONE,
        payload: res.data
      });
    })
    .catch(error => {
      dispatch({
        type: RESEARCH_ERROR,
        error: error
      });
    });
};

/*********************
 * * STORE RESEARCH
 *********************/
export const storeResearch = payload => ({
  type: RESEARCH_STORE,
  payload
});

/*********************
 * * SAVE RESEARCH
 *********************/
export const saveResearch = (payload, cb) => dispatch => {
  console.log("payload", payload);
  axios
    .post(`${HOST}/api/researches`, payload)
    .then(res => {
      dispatch({
        type: RESEARCH_SAVE,
        payload: res.data
      });
      if (typeof cb === "function") cb(res.data);
    })
    .catch(error => {
      dispatch({
        type: RESEARCH_ERROR,
        error: error.response.data
      });
    });
};

/***********************
 * * UPDATE RESEARCH
 ***********************/
export const updateResearch = (payload, cb) => dispatch => {
  if (payload.file) {
    const formData = new FormData();
    Object.keys(payload).map(key => formData.append(key, payload[key]));
    const config = {
      headers: {
        "content-type": "multipart/form-data"
      }
    };
    axios
      .put(`${HOST}/api/researches/upload`, formData, config)
      .then(() => cb())
      .catch(error => {
        dispatch({
          type: RESEARCH_ERROR,
          error: error.response.data
        });
      });
  } else {
    axios
      .put(`${HOST}/api/researches`, payload)
      .then(() => cb())
      .catch(error => {
        dispatch({
          type: RESEARCH_ERROR,
          error: error.response.data
        });
      });
  }
};
/**
 * * GET ALL RESEARCHES
 */
export const getAllResearches = () => dispatch => {
  dispatch({
    type: RESEARCH_LOADING
  });
  axios
    .post(`${HOST}/api/researches/all`)
    .then(res => {
      dispatch({
        type: RESEARCH_GET_ALL,
        payload: res.data
      });
    })
    .catch(error => {
      dispatch({
        type: RESEARCH_ERROR,
        error: error
      });
    });
};
/**
 * * GET RESEARCH BY ID
 */
export const getResearchById = id => dispatch => {
  dispatch({
    type: RESEARCH_LOADING
  });
  axios
    .post(`${HOST}/api/researches/${id}`)
    .then(res => {
      dispatch({
        type: RESEARCH_STORE,
        payload: res.data
      });
    })
    .catch(error =>
      dispatch({
        type: RESEARCH_ERROR,
        error
      })
    );
};

/**
 * * CLEAR RESEARCH
 */
export const clearResearch = () => ({
  type: RESEARCH_STORE,
  payload: {}
});

export default reducer;
