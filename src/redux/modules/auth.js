import axios from 'axios'
import jwt_decode from 'jwt-decode'
import { Record } from 'immutable'
// useful tools
import { setAuthToken } from '../../helpers/authHelpers'
import isEmpty from '../../helpers/is-empty'
// --- APP NAME ---
import { appName, HOST } from '../../config'

// --- MODULE NAME ---
export const moduleName = 'auth'

//? --- ACTIONS ---
const AUTH_LOADING = `${appName}/${moduleName}/LOADING`
const AUTH_GET_ALL_USERS = `${appName}/${moduleName}/GET_ALL_USERS`
const AUTH_GET_USER = `${appName}/${moduleName}/GET_USER`
const AUTH_GET_ERROR = `${appName}/${moduleName}/GET_ERROR`

export const AUTH_SET_USER = `${appName}/${moduleName}/SET_USER`

//? --- INITIAL STATE ---
const ReducerRecord = Record({
  isAuthenticated: false,
  users: [],
  editedUser: {},
  user: {},
  error: {},
  loading: false
})

//! --- REDUCER ---
const reducer = (state = new ReducerRecord(), action) => {
  const { type, payload, error } = action

  switch (type) {
    case AUTH_LOADING:
      return state.set('loading', true)
    case AUTH_SET_USER:
      return state
        .set('loading', false)
        .set('isAuthenticated', !isEmpty(payload))
        .set('user', payload)
        .set('error', {})
    case AUTH_GET_ALL_USERS:
      return state
        .set('loading', false)
        .set('users', payload)
        .set('error', {})
    case AUTH_GET_USER:
      return state
        .set('loading', false)
        .set('editedUser', payload)
        .set('error', {})
    case AUTH_GET_ERROR:
      return state.set('loading', false).set('error', error.response.data)
    default:
      return state
  }
}

//! --- AC ---
/**
 * * LOGIN
 */
export const loginUser = (userData, history) => dispatch => {
  axios
    .post(`${HOST}/api/users/login`, userData)
    .then(res => {
      const { token } = res.data

      localStorage.setItem('jwtToken', token)
      setAuthToken(token)

      dispatch({
        type: AUTH_SET_USER,
        payload: jwt_decode(token)
      })

      history.push('/admin/dashboard')
    })
    .catch(error =>
      dispatch({
        type: AUTH_GET_ERROR,
        error
      })
    )
}

/**
 * * LOGOUT
 */
export const logoutUser = () => dispatch => {
  localStorage.removeItem('jwtToken')
  setAuthToken(false)
  dispatch({
    type: AUTH_SET_USER,
    payload: {}
  })
}

/**
 * * GET ALL USERS
 */
export const getUsers = () => dispatch => {
  dispatch({
    type: AUTH_LOADING
  })
  axios.post(`${HOST}/api/users/all`).then(res =>
    dispatch({
      type: AUTH_GET_ALL_USERS,
      payload: res.data
    })
  )
}

/**
 * * GET USER BY ID
 */
export const getUserById = _id => dispatch => {
  dispatch({
    type: AUTH_LOADING
  })
  axios.post(`${HOST}/api/users/${_id}`).then(res =>
    dispatch({
      type: AUTH_GET_USER,
      payload: res.data
    })
  )
}

/**
 * * STORE USER
 */
export const storeUser = payload => ({
  type: AUTH_GET_USER,
  payload
})

/**
 * * REGISTER NEW USER
 */
export const registerUser = (userData, cb) => dispatch => {
  axios
    .post(`${HOST}/api/users/register`, userData)
    .then(() => cb())
    .catch(error =>
      dispatch({
        type: AUTH_GET_ERROR,
        error
      })
    )
}

/**
 * * SAVE EXISTED USER
 */
export const updateUser = (userData, cb) => dispatch => {
  axios
    .put(`${HOST}/api/users`, userData)
    .then(() => cb())
    .catch(error =>
      dispatch({
        type: AUTH_GET_ERROR,
        error
      })
    )
}

export default reducer
