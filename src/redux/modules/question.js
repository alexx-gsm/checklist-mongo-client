import axios from 'axios'
import { Record } from 'immutable'
import { appName, HOST } from '../../config'

// mocks
import answerMocks from '../mocks/answers'

// --- MODULE NAME ---
export const moduleName = 'questionStore'

// --- ACTIONS ---
export const QUESTION_LOADING = `${appName}/${moduleName}/QUESTION_LOADING`
export const QUESTION_STORE = `${appName}/${moduleName}/QUESTION_STORE`
export const QUESTION_SAVE = `${appName}/${moduleName}/QUESTION_SAVE`
export const QUESTION_GET_ALL = `${appName}/${moduleName}/QUESTION_GET_ALL`
export const QUESTION_ADD_CRITERIA = `${appName}/${moduleName}/QUESTION_ADD_CRITERIA`
export const QUESTION_ERROR = `${appName}/${moduleName}/QUESTION_ERROR`

const initialQuestion = {
  title: '',
  target: 'common',
  answerId: '',
  criterias: []
}
// --- INITIAL STATE ---
const ReducerRecord = Record({
  questions: [],
  question: initialQuestion,
  answers: answerMocks,
  error: {},
  loading: false
})

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), { type, payload, error }) => {
  switch (type) {
    case QUESTION_LOADING:
      return state.set('loading', true)
    case QUESTION_STORE:
      return state
        .set('loading', false)
        .set('question', payload)
        .set('error', {})
    case QUESTION_ADD_CRITERIA:
      return state
        .set('loading', false)
        .set('question', {
          ...state.question,
          criterias: [...state.question.criterias, payload]
        })
        .set('error', {})
    case QUESTION_GET_ALL:
      return state
        .set('loading', false)
        .set('questions', payload)
        .set('error', {})
    case QUESTION_ERROR:
      return state.set('loading', false).set('error', error)
    default:
      return state
  }
}

// --- AC ---
/**********************
 * * STORE QUESTION
 **********************/
export const storeQuestion = payload => ({
  type: QUESTION_STORE,
  payload
})

/**********************
 * * SAVE QUESTION
 **********************/
export const saveQuestion = (payload, cb) => dispatch => {
  axios
    .post(`${HOST}/api/questions`, payload)
    .then(() => cb())
    .catch(error => {
      dispatch({
        type: QUESTION_ERROR,
        error: error.response.data
      })
    })
}

/**********************
 * * GET ALL QUESTIONS
 * ? @param sid = survey Id
 **********************/
export const getQuestions = sid => dispatch => {
  dispatch({
    type: QUESTION_LOADING
  })
  axios
    .post(`${HOST}/api/questions/all`, { sid })
    .then(res => {
      dispatch({
        type: QUESTION_GET_ALL,
        payload: res.data
      })
    })
    .catch(error => {
      dispatch({
        type: QUESTION_ERROR,
        error
      })
    })
}

/**
 * * GET QUESTION BY ID
 */
export const getQuestionById = id => dispatch => {
  dispatch({
    type: QUESTION_LOADING
  })
  axios
    .post(`${HOST}/api/questions/${id}`)
    .then(res => {
      dispatch({
        type: QUESTION_STORE,
        payload: res.data
      })
    })
    .catch(error =>
      dispatch({
        type: QUESTION_ERROR,
        error
      })
    )
}

/************************************************
 * * @desc    ADD NEW CRITERIA TO QUESTION
 * ? @params  payload: new criteria
 ************************************************/
export const addNewCriteria = payload => ({
  type: QUESTION_ADD_CRITERIA,
  payload
})

/**
 * * UPLOAD CRITERIAS
 */
export const uploadCriterias = payload => dispatch => {
  if (payload.file) {
    const formData = new FormData()
    Object.keys(payload).map(key => formData.append(key, payload[key]))
    const config = {
      headers: {
        'content-type': 'multipart/form-data'
      }
    }
    dispatch({
      type: QUESTION_LOADING
    })
    axios
      .post(`${HOST}/api/questions/upload/criterias`, formData, config)
      .then(res =>
        dispatch({
          type: QUESTION_STORE,
          payload: res.data
        })
      )
      .catch(error => {
        dispatch({
          type: QUESTION_ERROR,
          error
        })
      })
  }
}

/**
 * * CLEAR QUESTION
 */
export const clearQuestion = () => ({
  type: QUESTION_STORE,
  payload: initialQuestion
})

export default reducer
