import axios from 'axios'
import { Record } from 'immutable'
import { appName, HOST } from '../../config'

import { QUESTION_ADD_CRITERIA } from './question'

//! --- MODULE NAME ---
export const moduleName = 'criteriaStore'

//! --- ACTIONS ---
export const CRITERIA_LOADING = `${appName}/${moduleName}/CRITERIA_LOADING`
export const CRITERIA_STORE = `${appName}/${moduleName}/CRITERIA_STORE`
export const CRITERIA_SAVE = `${appName}/${moduleName}/CRITERIA_SAVE`
export const CRITERIA_GET_ALL = `${appName}/${moduleName}/CRITERIA_GET_ALL`
export const CRITERIA_UPLOAD = `${appName}/${moduleName}/CRITERIA_UPLOAD`
export const CRITERIA_ERROR = `${appName}/${moduleName}/CRITERIA_ERROR`

//! --- INITIAL STATE ---
const ReducerRecord = Record({
  criterias: [],
  criteria: {},
  uploadedCriterias: {},
  error: {},
  loading: false
})

//! --- REDUCER ---
const reducer = (state = new ReducerRecord(), { type, payload, error }) => {
  switch (type) {
    case CRITERIA_LOADING:
      return state.set('loading', true)
    case CRITERIA_STORE:
      return state
        .set('loading', false)
        .set('criteria', payload)
        .set('error', {})
    case CRITERIA_GET_ALL:
      return state
        .set('loading', false)
        .set('criterias', payload)
        .set('error', {})
    case CRITERIA_UPLOAD:
      return state
        .set('loading', false)
        .set('uploadedCriterias', payload)
        .set('error', {})
    case CRITERIA_ERROR:
      return state.set('loading', false).set('error', error)
    default:
      return state
  }
}

//! --- AC ---

/************************************************
 * * @desc    STORE CRITERIA
 * ? @params  payload
 ************************************************/
export const storeCriteria = payload => ({
  type: CRITERIA_STORE,
  payload
})

/************************************************
 * * @desc    SAVE CRITERIA
 * ? @params  payload
 * ? @params  cb: callback function
 ************************************************/
export const saveCriteria = (payload, cb) => dispatch => {
  axios
    .post(`${HOST}/api/criterias`, payload)
    .then(res => {
      if (typeof cb === 'function') cb(res.data)
    })
    .catch(error => {
      dispatch({
        type: CRITERIA_ERROR,
        error: error.response.data
      })
    })
}

/************************************************
 * * @desc    GET ALL CRITERIAS
 ************************************************/
export const getCriterias = rid => dispatch => {
  dispatch({
    type: CRITERIA_LOADING
  })
  axios
    .post(`${HOST}/api/criterias/all`, { rid })
    .then(res => {
      dispatch({
        type: CRITERIA_GET_ALL,
        payload: res.data
      })
    })
    .catch(error => {
      dispatch({
        type: CRITERIA_ERROR,
        error: error
      })
    })
}

/************************************************
 * * @desc    GET ONE CRITERIA
 * ? @param   _id: criteria id
 ************************************************/
export const getOneCriteria = _id => dispatch => {
  dispatch({
    type: CRITERIA_LOADING
  })
  axios
    .post(`${HOST}/api/criterias/${_id}`)
    .then(res => {
      dispatch({
        type: CRITERIA_STORE,
        payload: res.data
      })
    })
    .catch(error =>
      dispatch({
        type: CRITERIA_ERROR,
        error
      })
    )
}

/**
 * * CRITERIA_UPLOAD
 */
export const uploadCriterias = payload => dispatch => {
  if (payload.file) {
    const formData = new FormData()
    Object.keys(payload).map(key => formData.append(key, payload[key]))
    const config = {
      headers: {
        'content-type': 'multipart/form-data'
      }
    }
    axios
      .put(`${HOST}/api/criterias/upload`, formData, config)
      .then(res =>
        dispatch({
          type: CRITERIA_UPLOAD,
          payload: res.data
        })
      )
      .catch(error => {
        dispatch({
          type: CRITERIA_ERROR,
          error
        })
      })
  }
}

/**
 * * CLEAR CRITERIA
 */
export const clearCriteria = () => ({
  type: CRITERIA_STORE,
  payload: {}
})

/**
 * * CLEAR UPLOADED CRITERIAS
 */
export const clearUploadedCriterias = () => ({
  type: CRITERIA_UPLOAD,
  payload: {}
})

export default reducer
