import axios from 'axios'
import { Record } from 'immutable'
import { appName, HOST } from '../../config'

//! --- MODULE NAME ---
export const moduleName = 'resultStore'

//! --- ACTIONS ---
export const RESULT_LOADING = `${appName}/${moduleName}/RESULT_LOADING`
export const RESULT_STORE = `${appName}/${moduleName}/RESULT_STORE`
export const RESULT_STORE_SUMMARY = `${appName}/${moduleName}/RESULT_STORE_SUMMARY`
export const RESULT_SAVE = `${appName}/${moduleName}/RESULT_SAVE`
export const RESULT_GET_ALL = `${appName}/${moduleName}/RESULT_GET_ALL`
export const RESULT_GET_DATA = `${appName}/${moduleName}/RESULT_GET_DATA`
export const RESULT_UPLOAD = `${appName}/${moduleName}/RESULT_UPLOAD`
export const RESULT_ERROR = `${appName}/${moduleName}/RESULT_ERROR`

//! --- INITIAL STATE ---
const ReducerRecord = Record({
  results: [],
  result: {},
  data: {},
  error: {},
  loading: false
})

//! --- REDUCER ---
const reducer = (state = new ReducerRecord(), { type, payload, error }) => {
  switch (type) {
    case RESULT_LOADING:
      return state.set('loading', true)
    case RESULT_STORE:
      return state
        .set('loading', false)
        .set('result', payload)
        .set('error', {})
    case RESULT_STORE_SUMMARY:
      return state
        .set('loading', false)
        .set('result', { ...state.result, summary: payload })
        .set('error', {})
    case RESULT_GET_ALL:
      return state
        .set('loading', false)
        .set('results', payload)
        .set('error', {})
    case RESULT_GET_DATA:
      return state
        .set('loading', false)
        .set('data', payload)
        .set('error', {})

    case RESULT_ERROR:
      return state.set('loading', false).set('error', error)
    default:
      return state
  }
}

//! --- AC ---
/************************************************
 * * @desc    GET DATA
 * ? @param   _id: result id
 ************************************************/
export const getData = link => dispatch => {
  dispatch({
    type: RESULT_LOADING
  })
  axios
    .post(`${HOST}/api/results/data/${link}`)
    .then(res => {
      dispatch({
        type: RESULT_GET_DATA,
        payload: res.data
      })
    })
    .catch(error =>
      dispatch({
        type: RESULT_ERROR,
        error
      })
    )
}

/************************************************
 * * @desc    STORE RESULT
 * ? @params  payload
 ************************************************/
export const storeResult = payload => ({
  type: RESULT_STORE,
  payload
})

/************************************************
 * * @desc    SAVE RESULT
 * ? @params  payload
 * ? @params  cb: callback function
 ************************************************/
export const saveResult = (payload, cb) => dispatch => {
  axios
    .post(`${HOST}/api/results`, payload)
    .then(res => {
      dispatch({
        type: RESULT_STORE,
        payload: res.data
      })
      if (typeof cb === 'function') cb(res.data)
    })
    .catch(error => {
      dispatch({
        type: RESULT_ERROR,
        error: error.response.data
      })
    })
}

/************************************************
 * * @desc    GET ALL RESULTS
 ************************************************/
export const getResults = sid => dispatch => {
  dispatch({
    type: RESULT_LOADING
  })
  axios
    .post(`${HOST}/api/results/all`, { sid })
    .then(res => {
      dispatch({
        type: RESULT_GET_ALL,
        payload: res.data
      })
    })
    .catch(error => {
      dispatch({
        type: RESULT_ERROR,
        error: error
      })
    })
}

/************************************************
 * * @desc    GET ONE RESULT
 * ? @param   _id: result id
 ************************************************/
export const getOneResult = _id => dispatch => {
  dispatch({
    type: RESULT_LOADING
  })
  axios
    .post(`${HOST}/api/results/${_id}`)
    .then(res => {
      dispatch({
        type: RESULT_STORE,
        payload: res.data
      })
    })
    .catch(error =>
      dispatch({
        type: RESULT_ERROR,
        error
      })
    )
}

/************************************************
 * * @desc    STORE SUMMARY
 * ? @param   payload: summary
 ************************************************/
export const storeSummaryResult = payload => ({
  type: RESULT_STORE_SUMMARY,
  payload
})

export default reducer
