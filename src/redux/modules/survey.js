import axios from 'axios'
import { Record } from 'immutable'
import { appName, HOST } from '../../config'

// --- MODULE NAME ---
export const moduleName = 'surveyStore'

// --- ACTIONS ---
export const SURVEY_LOADING = `${appName}/${moduleName}/SURVEY_LOADING`
export const SURVEY_STORE = `${appName}/${moduleName}/SURVEY_STORE`
export const SURVEY_SAVE = `${appName}/${moduleName}/SURVEY_SAVE`
export const SURVEY_GET_ALL = `${appName}/${moduleName}/SURVEY_GET_ALL`
export const SURVEY_SET_STAGE = `${appName}/${moduleName}/SURVEY_SET_STAGE`
export const SURVEY_STORE_STAGE_RESULT = `${appName}/${moduleName}/SURVEY_STORE_STAGE_RESULT`
export const SURVEY_STORE_PREV_RESULT = `${appName}/${moduleName}/SURVEY_STORE_PREV_RESULT`
export const SURVEY_ERROR = `${appName}/${moduleName}/SURVEY_ERROR`

const initialSurvey = {}
// --- INITIAL STATE ---
const ReducerRecord = Record({
  surveys: [],
  survey: {},
  stage: 0,
  result: [],
  prevResult: [],
  error: {},
  loading: false
})

//! --- REDUCER ---
const reducer = (state = new ReducerRecord(), { type, payload, error }) => {
  switch (type) {
    case SURVEY_LOADING:
      return state.set('loading', true)
    case SURVEY_STORE:
      return state
        .set('loading', false)
        .set('survey', payload)
        .set('error', {})
    case SURVEY_GET_ALL:
      return state
        .set('loading', false)
        .set('surveys', payload)
        .set('error', {})
    case SURVEY_SET_STAGE:
      return state
        .set('loading', false)
        .set('stage', payload)
        .set('error', {})
    case SURVEY_STORE_STAGE_RESULT:
      return state
        .set('loading', false)
        .set('result', payload)
        .set('error', {})
    case SURVEY_STORE_PREV_RESULT:
      return state
        .set('loading', false)
        .set('prevResult', payload)
        .set('error', {})
    case SURVEY_ERROR:
      return state.set('loading', false).set('error', error)
    default:
      return state
  }
}

//! --- AC ---
/**
 * * STORE SURVEY
 */
export const storeSurvey = payload => ({
  type: SURVEY_STORE,
  payload
})
/**
 * * STORE SURVEY AND SAVE
 */
export const storeAndUpdateSurvey = payload => dispatch => {
  console.log('AC --- STORE SURVEY AND SAVE')
  axios
    .post(`${HOST}/api/surveys`, payload)
    .then(() => {
      dispatch({
        type: SURVEY_STORE,
        payload
      })
    })
    .catch(error => {
      dispatch({
        type: SURVEY_ERROR,
        error
      })
    })
}

/**
 * * SAVE SURVEY
 */
export const saveSurvey = (payload, cb) => dispatch => {
  console.log('AC --- SAVE SURVEY')
  axios
    .post(`${HOST}/api/surveys`, payload)
    .then(res => {
      dispatch({
        type: SURVEY_STORE,
        payload: res.data
      })
      if (typeof cb === 'function') cb(res.data)
    })
    .catch(error => {
      dispatch({
        type: SURVEY_ERROR,
        error
      })
    })
}
/**
 * * UPDATE SURVEY
 */
export const updateSurvey = (payload, cb) => dispatch => {
  console.log('AC --- PUT SURVEY')
  axios
    .put(`${HOST}/api/surveys`, payload)
    .then(() => cb())
    .catch(error => {
      dispatch({
        type: SURVEY_ERROR,
        error
      })
    })
}
/**
 * * GET ALL SURVEYS
 */
export const getSurveys = rid => dispatch => {
  dispatch({
    type: SURVEY_LOADING
  })
  axios
    .post(`${HOST}/api/surveys/all`, { rid })
    .then(res => {
      console.log('AC --- GET ALL SURVEYS: ok')
      dispatch({
        type: SURVEY_GET_ALL,
        payload: res.data
      })
    })
    .catch(error => {
      console.log('AC --- GET ALL SURVEYS: err')
      dispatch({
        type: SURVEY_ERROR,
        error
      })
    })
}
/**
 * * GET SURVEY BY ID
 * ? @param rid = research Id
 * ? @param  id = survey Id
 */
export const getOneSurvey = id => dispatch => {
  dispatch({
    type: SURVEY_LOADING
  })
  axios
    .post(`${HOST}/api/surveys/${id}`)
    .then(res => {
      dispatch({
        type: SURVEY_STORE,
        payload: res.data
      })
    })
    .catch(error =>
      dispatch({
        type: SURVEY_ERROR,
        error
      })
    )
}

export const setSurveyStage = payload => ({
  type: SURVEY_SET_STAGE,
  payload
})

export const storeStageResult = payload => ({
  type: SURVEY_STORE_STAGE_RESULT,
  payload
})

export const storePrevResult = payload => ({
  type: SURVEY_STORE_PREV_RESULT,
  payload
})

/**
 * * clear survey
 */
export const clearSurvey = () => ({
  type: SURVEY_STORE,
  payload: initialSurvey
})

export default reducer
