import axios from 'axios'
import { Record } from 'immutable'
import { appName, HOST } from '../../config'

// --- MODULE NAME ---
export const moduleName = 'respondentStore'

// --- ACTIONS ---
export const RESPONDENT_LOADING = `${appName}/${moduleName}/RESPONDENT_LOADING`
export const RESPONDENT_STORE = `${appName}/${moduleName}/RESPONDENT_STORE`
export const RESPONDENT_SAVE = `${appName}/${moduleName}/RESPONDENT_SAVE`
export const RESPONDENT_GET_ALL = `${appName}/${moduleName}/RESPONDENT_GET_ALL`
export const RESPONDENT_GET_ONE = `${appName}/${moduleName}/RESPONDENT_GET_ONE`
export const RESPONDENT_UPLOAD = `${appName}/${moduleName}/RESPONDENT_UPLOAD`
export const RESPONDENT_ERROR = `${appName}/${moduleName}/RESPONDENT_ERROR`

// --- INITIAL STATE ---
const ReducerRecord = Record({
  respondents: [],
  respondent: {},
  error: {},
  loading: false
})

//! --- REDUCER ---
const reducer = (state = new ReducerRecord(), { type, payload, error }) => {
  switch (type) {
    case RESPONDENT_LOADING:
      return state.set('loading', true)
    case RESPONDENT_STORE:
      return state
        .set('loading', false)
        .set('respondent', payload)
        .set('error', {})
    case RESPONDENT_GET_ALL:
      return state
        .set('loading', false)
        .set('respondents', payload)
        .set('error', {})
    case RESPONDENT_ERROR:
      return state.set('loading', false).set('error', error)
    default:
      return state
  }
}

//! --- AC ---
/***********************************
 * * @desc    STORE RESPONDENT
 * ? @param   payload
 ***********************************/
export const storeRespondent = payload => ({
  type: RESPONDENT_STORE,
  payload
})

/***********************************
 * * @desc    SAVE RESPONDENT
 * ? @param   payload
 * ? @param   cb: callback function
 ***********************************/
export const saveRespondent = (payload, cb) => dispatch => {
  axios
    .post(`${HOST}/api/respondents`, payload)
    .then(res => {
      dispatch({
        type: RESPONDENT_STORE,
        payload: res.data
      })
      if (typeof cb === 'function') cb(res.data)
    })
    .catch(error => {
      dispatch({
        type: RESPONDENT_ERROR,
        error: error.response.data
      })
    })
}

/***********************************
 * * @desc    GET ALL RESPONDENTS
 * ? @param   sid: survey Id
 ***********************************/
export const getRespondents = sid => dispatch => {
  dispatch({
    type: RESPONDENT_LOADING
  })
  axios
    .post(`${HOST}/api/respondents/all`, { sid })
    .then(res => {
      dispatch({
        type: RESPONDENT_GET_ALL,
        payload: res.data
      })
    })
    .catch(error => {
      dispatch({
        type: RESPONDENT_ERROR,
        error
      })
    })
}

/***********************************
 * * @desc    GET RESPONDENT BY ID
 * ? @param   _id: respondent Id
 ***********************************/
export const getOneRespondent = _id => dispatch => {
  dispatch({
    type: RESPONDENT_LOADING
  })
  axios
    .post(`${HOST}/api/respondents/${_id}`)
    .then(res => {
      dispatch({
        type: RESPONDENT_STORE,
        payload: res.data
      })
    })
    .catch(error =>
      dispatch({
        type: RESPONDENT_ERROR,
        error
      })
    )
}

/***********************************
 * * @desc    UPLOAD MULTIPLE RESPONDENTS
 * ? @param   respondents: array of respondents
 * ? @param   callback function
 ***********************************/
export const uploadRespondents = (respondents, sid, cb) => dispatch => {
  dispatch({
    type: RESPONDENT_LOADING
  })
  axios
    .post(`${HOST}/api/respondents/upload`, { respondents, sid })
    .then(res => {
      dispatch({
        type: RESPONDENT_GET_ALL,
        payload: res.data
      })
      if (typeof cb === 'function') cb(res.data)
    })
    .catch(error =>
      dispatch({
        type: RESPONDENT_ERROR,
        error
      })
    )
}

export default reducer
