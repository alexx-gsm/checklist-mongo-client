import { Record } from 'immutable'
import { appName } from '../../config'

//! --- MODULE NAME ---
export const moduleName = 'shareStore'

//! --- ACTIONS ---
export const SHARE_STORE_SHARE = `${appName}/${moduleName}/SHARE_STORE_SHARE`
export const SHARE_STORE_SURVEY = `${appName}/${moduleName}/SHARE_STORE_SURVEY`

//! --- INITIAL STATE ---
const ReducerRecord = Record({
  share: {}
})

//! --- REDUCER ---
const reducer = (state = new ReducerRecord(), { type, payload }) => {
  switch (type) {
    case SHARE_STORE_SHARE:
      return state.set('share', { ...state.share, ...payload })
    default:
      return state
  }
}

//! --- AC ---
/**
 * * STORE SHARE
 */
export const storeShare = payload => ({
  type: SHARE_STORE_SHARE,
  payload
})

export default reducer
