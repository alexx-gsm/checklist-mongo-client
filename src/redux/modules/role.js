import axios from 'axios'
import { Record } from 'immutable'
import { appName } from '../../config'

// --- MODULE NAME ---
export const moduleName = 'roleStore'

// --- ACTIONS ---
export const ROLE_LOADING = `${appName}/${moduleName}/ROLE_LOADING`
export const ROLE_STORE = `${appName}/${moduleName}/ROLE_STORE`
export const ROLE_SAVE = `${appName}/${moduleName}/ROLE_SAVE`
export const ROLE_GET_ALL = `${appName}/${moduleName}/ROLE_GET_ALL`
export const ROLE_ERROR = `${appName}/${moduleName}/ROLE_ERROR`

// --- INITIAL STATE ---
const ReducerRecord = Record({
  roles: [
    {
      title: 'Администратор',
      alias: 'admin'
    },
    {
      title: 'Редактор',
      alias: 'editor'
    },
    {
      title: 'Пользователь',
      alias: 'user'
    }
  ],
  role: {},
  error: {},
  loading: false
})

// --- REDUCER ---
const reducer = (state = new ReducerRecord(), { type, payload, error }) => {
  switch (type) {
    case ROLE_LOADING:
      return state.set('loading', true)
    case ROLE_STORE:
      return state
        .set('loading', false)
        .set('role', payload)
        .set('error', {})
    case ROLE_GET_ALL:
      return state
        .set('loading', false)
        .set('roles', payload)
        .set('error', {})
    case ROLE_ERROR:
      return state.set('loading', false).set('error', error)
    default:
      return state
  }
}

// --- AC ---

export default reducer
