export default [
  {
    _id: '1',
    title: 'Связи',
    type: 'links',
    variants: []
  },
  {
    _id: '2',
    title: 'Оценка критерия (5)',
    type: 'test',
    variants: [
      {
        _id: '1',
        title: 'Полностью не согласен',
        weight: '1'
      },
      {
        _id: '2',
        title: 'Не согласен',
        weight: '2'
      },
      {
        _id: '3',
        title: 'Затрудняюсь ответить',
        weight: '3'
      },
      {
        _id: '4',
        title: 'Согласен',
        weight: '4'
      },
      {
        _id: '5',
        title: 'Полностью согласен',
        weight: '5'
      }
    ]
  },
  {
    _id: '3',
    title: 'Оценка сотрудника (10)',
    type: 'employeeTest',
    variants: [
      {
        _id: '1',
        title: 'Хуже некуда',
        weight: '1'
      },
      {
        _id: '2',
        title: 'Очень плохо',
        weight: '2'
      },
      {
        _id: '3',
        title: 'Плохо',
        weight: '3'
      },
      {
        _id: '4',
        title: 'Хуже среднего',
        weight: '4'
      },
      {
        _id: '5',
        title: 'Посредственно',
        weight: '5'
      },
      {
        _id: '6',
        title: 'Лучше среднего',
        weight: '6'
      },
      {
        _id: '7',
        title: 'Хорошо',
        weight: '7'
      },
      {
        _id: '8',
        title: 'Очень хорошо',
        weight: '8'
      },
      {
        _id: '9',
        title: 'Отлично',
        weight: '9'
      },
      {
        _id: '10',
        title: 'Превосходно',
        weight: '10'
      }
    ]
  },
  {
    _id: '4',
    title: 'Произвольный',
    type: 'free'
  }
]
