import { combineReducers } from 'redux'
import shareReducer, { moduleName as shareModule } from './modules/share'
import authReducer, { moduleName as authModule } from './modules/auth'
import sidebarReducer, { moduleName as sidebarModule } from './modules/sidebar'
import roleReducer, { moduleName as roleModule } from './modules/role'
import researchReducer, {
  moduleName as researchModule
} from './modules/research'
import surveyReducer, { moduleName as surveyModule } from './modules/survey'
import questionReducer, {
  moduleName as questionModule
} from './modules/question'
import criteriaReducer, {
  moduleName as criteriaModule
} from './modules/criteria'
import respondentReducer, {
  moduleName as respondentModule
} from './modules/respondent'
import resultReducer, { moduleName as resultModule } from './modules/result'

export default combineReducers({
  [shareModule]: shareReducer,
  [sidebarModule]: sidebarReducer,
  [authModule]: authReducer,
  [roleModule]: roleReducer,
  [researchModule]: researchReducer,
  [surveyModule]: surveyReducer,
  [questionModule]: questionReducer,
  [criteriaModule]: criteriaReducer,
  [respondentModule]: respondentReducer,
  [resultModule]: resultReducer
})
