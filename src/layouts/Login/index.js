import { compose, withState, withHandlers } from 'recompose';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
// AC
import { loginUser } from '../../redux/modules/auth';
import { withStyles } from '@material-ui/core/styles';
import Login from './Login';
// styles
import styles from './styles';

export default compose(
  connect(
    null,
    { loginUser }
  ),
  withRouter,
  withState('userData', 'setUserData', {}),
  withState('isPasswordVisible', 'setPasswordVisible', false),
  withHandlers({
    onChange: ({ userData, setUserData }) => event =>
      setUserData({
        ...userData,
        [event.target.name]: event.target.value
      }),
    onShowPassword: ({ setPasswordVisible }) => () => setPasswordVisible(true),
    onLogin: ({ userData, loginUser, history }) => () =>
      loginUser(userData, history)
  }),
  withStyles(styles, { withTheme: true })
)(Login);
