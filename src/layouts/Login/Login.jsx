import React from 'react';
// material-ui components
import FormControl from '@material-ui/core/FormControl';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Divider from '@material-ui/core/Divider';
// @material-ui/icons
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import AccountCircle from '@material-ui/icons/AccountCircle';

const Login = ({
  classes,
  userData,
  onChange,
  onLogin,
  isPasswordVisible,
  setPasswordVisible
}) => {
  const { email, password } = userData;
  return (
    <Grid
      container
      className={classes.root}
      alignItems="center"
      justify="center"
    >
      <Paper className={classes.container} elevation={1}>
        <form className={classes.form} noValidate autoComplete="off">
          <Grid container justify="center">
            <AccountCircle className={classes.avatar} />
          </Grid>

          <TextField
            required
            id="required"
            label="Login"
            className={classes.textField}
            margin="normal"
            value={email}
            name="email"
            onChange={onChange}
          />

          <FormControl className={classes.textField}>
            <InputLabel htmlFor="adornment-password">Password</InputLabel>
            <Input
              required
              id="adornment-password"
              type={isPasswordVisible ? 'text' : 'password'}
              value={password}
              name="password"
              onChange={onChange}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="Toggle password visibility"
                    onClick={setPasswordVisible}
                    // onMouseDown={this.handleMouseDownPassword}
                  >
                    {isPasswordVisible ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              }
            />
          </FormControl>
          <Divider />
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={onLogin}
          >
            Login
          </Button>
        </form>
      </Paper>
    </Grid>
  );
};

export default Login;
