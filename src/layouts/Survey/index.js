import { compose, withHandlers } from 'recompose'
import { connect } from 'react-redux'
// AC
import {
  storeStageResult,
  setSurveyStage,
  storeAndUpdateSurvey
} from '../../redux/modules/survey'
import { saveResult } from '../../redux/modules/result'
// hocs
import withSidebar from '../../hocs/withSidebar'
import { withStyles } from '@material-ui/core/styles'
// component
import Survey from './Survey'
// styles
import styles from './styles'
// useful tools
import isEmpty from '../../helpers/is-empty'

export default compose(
  connect(
    ({ surveyStore, questionStore, respondentStore, resultStore }) => {
      const { stage, survey, result } = surveyStore
      const { questions } = questionStore
      const { respondent } = respondentStore
      return {
        survey,
        stage,
        result,
        questionList: questions,
        questions,
        question:
          !isEmpty(questions) && stage < questions.length
            ? questions[stage]
            : {},
        respondent,
        _result: resultStore.result
      }
    },
    { storeStageResult, setSurveyStage, storeAndUpdateSurvey, saveResult }
  ),
  withHandlers({
    handleClick: ({
      result,
      setSurveyStage,
      storeStageResult,
      _result,
      question,
      questions,
      saveResult
    }) => index => () => {
      const { summary = {} } = _result

      saveResult({
        ..._result,
        summary: {
          ...summary,
          [question._id]: result
        }
      })

      const qid = questions && questions[index] ? questions[index]._id : null

      const nextResult =
        qid && _result && _result.summary && _result.summary[qid]
          ? _result.summary[qid]
          : []

      storeStageResult(nextResult)
      setSurveyStage(index)
    },
    handleFinish: ({
      result,
      _result,
      question,
      saveResult,
      history
    }) => () => {
      const { summary = {} } = _result

      saveResult({
        ..._result,
        summary: {
          ...summary,
          [question._id]: result
        }
      })

      history.push('/')
    }
  }),
  withStyles(styles, { withTheme: true }),
  withSidebar
)(Survey)
