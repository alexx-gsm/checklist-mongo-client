const drawerWidth = 200

export default theme => ({
  root: {
    flexGrow: 1,
    minHeight: '100%',
    // height: '100%',
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
    width: '100%',
    backgroundColor: theme.palette.background.default
  },
  toolbar: theme.mixins.toolbar,
  wrap: {
    height: '100%'
  },
  drawerPaper: {
    width: drawerWidth,
    height: '100%',
    [theme.breakpoints.up('md')]: {
      position: 'relative'
    }
  },
  Content: {
    margin: '0 auto',
    maxWidth: '1024px',
    width: '100%',
    backgroundColor: theme.palette.background.default,
    padding: theme.spacing.unit * 3,
    paddingBottom: '78px',
    boxSizing: 'border-box',
    [theme.breakpoints.down('sm')]: {
      padding: '0 0 52px'
    },
    [theme.breakpoints.down('xs')]: {
      padding: '0 0 54px'
    }
  },
  ListRoot: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
    position: 'relative',
    overflow: 'auto'
  },
  ListItemRoot: {
    borderBottom: `1px solid ${theme.palette.grey[400]}`
  },
  GridFinishButton: {
    padding: 20,
    borderBottom: `1px solid ${theme.palette.grey[400]}`
  }
})
