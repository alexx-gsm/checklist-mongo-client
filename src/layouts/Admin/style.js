const drawerWidth = 240

export default theme => ({
  root: {
    flexGrow: 1,
    minHeight: '100%',
    zIndex: 1,
    overflow: 'hidden',
    position: 'relative',
    display: 'flex',
    width: '100%'
  },
  toolbar: theme.mixins.toolbar,
  wrap: {
    height: '100%'
  },
  drawerPaper: {
    width: drawerWidth,
    height: '100%',
    [theme.breakpoints.up('md')]: {
      position: 'relative'
    }
  },
  content: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.default,
    padding: `0 ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px `,
    maxWidth: '100%',
    boxSizing: 'border-box',
    [theme.breakpoints.down('xs')]: {
      padding: 0
    }
  }
})
