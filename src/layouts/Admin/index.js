import { compose } from 'recompose'
// hocs
import withSidebar from '../../hocs/withSidebar'
import { withStyles } from '@material-ui/core/styles'
// component
import Admin from './Admin'
// styles
import styles from './style'

export default compose(
  withStyles(styles, { withTheme: true }),
  withSidebar
)(Admin)
