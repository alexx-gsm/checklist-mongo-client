import React from 'react'
import { object, bool, func } from 'prop-types'
import { Route, Switch, Redirect } from 'react-router-dom'
// material-ui components
import Drawer from '@material-ui/core/Drawer'
import Hidden from '@material-ui/core/Hidden'
// components
import AdminRoutes from '../../routes/AdminRoutes.jsx'
import Appbar from '../../components/Appbar'
import Sidebar from '../../components/Sidebar'

const switchRoutes = (
  <Switch>
    {AdminRoutes.map((prop, key) => {
      if (prop.redirect)
        return <Redirect from={prop.path} to={prop.to} key={key} />
      if (prop.divider) return ''
      return <Route path={prop.path} component={prop.component} key={key} />
    })}
  </Switch>
)

const Admin = ({ classes, theme, isOpen, onSidebarToggle }) => {
  return (
    <div className={classes.root}>
      <Appbar />
      <Hidden mdUp>
        <Drawer
          variant='temporary'
          anchor={theme.direction === 'rtl' ? 'right' : 'left'}
          open={isOpen}
          onClick={onSidebarToggle}
          classes={{
            paper: classes.drawerPaper
          }}
          ModalProps={{
            keepMounted: true // Better open performance on mobile.
          }}
        >
          <Sidebar />
        </Drawer>
      </Hidden>
      <Hidden smDown>
        <Drawer
          variant='permanent'
          open
          classes={{
            paper: classes.drawerPaper
          }}
        >
          <Sidebar />
        </Drawer>
      </Hidden>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        {switchRoutes}
      </main>
    </div>
  )
}

Admin.propTypes = {
  classes: object.isRequired,
  theme: object.isRequired,
  isOpen: bool.isRequired,
  onSidebarToggle: func.isRequired
}

export default Admin
