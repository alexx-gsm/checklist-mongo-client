import React from 'react'
import ReactDOM from 'react-dom'
import { checkAuthToken } from './helpers/authHelpers'
import store from './redux/store'
import { Provider } from 'react-redux'
import * as serviceWorker from './serviceWorker'
// components
import Routes from './routes'
// common styles
import './index.css'

// only for authorized users
checkAuthToken()

ReactDOM.render(
  <Provider store={store}>
    <Routes />
  </Provider>,
  document.getElementById('root')
)

serviceWorker.unregister()
