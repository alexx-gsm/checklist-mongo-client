export default theme => ({
  P20: { padding: '20px' },
  MT20: {
    marginTop: '20px'
  },
  MT40: {
    marginTop: '40px'
  },
  MB20: {
    marginBottom: '20px'
  },
  _Wrap: {
    maxWidth: '960px',
    padding: '20px 0'
  },
  _PageTitle: {
    color: theme.palette.grey[600],
    fontWeight: theme.typography.fontWeightLight,
    lineHeight: 1.2,
    padding: `${theme.spacing.unit * 2}px 0`,

    [theme.breakpoints.down('sm')]: {
      textAlign: 'center',
      padding: '0 5px'
    },
    [theme.breakpoints.down('xs')]: {
      fontSize: '2rem',
      paddingTop: '5px'
    }
  },
  // FORM
  _Form: {
    marginBottom: theme.spacing.unit * 4,
    [theme.breakpoints.down('sm')]: {
      padding: '0 20px'
    },
    [theme.breakpoints.down('xs')]: {
      padding: '0 8px'
    }
  },
  _ActionGrid: {
    [theme.breakpoints.down('sm')]: {
      padding: '0 20px'
    },
    [theme.breakpoints.down('xs')]: {
      padding: theme.spacing.unit,
      flexDirection: 'column'
    }
  },
  _ActionGridLeftAuto: {
    marginLeft: 'auto',
    [theme.breakpoints.down('xs')]: {
      marginLeft: 0
    }
  },
  _FormTitle: {
    fontSize: '46px',
    color: '#666',
    '& input': { padding: 0 },
    [theme.breakpoints.down('xs')]: {
      fontSize: 28
    }
  },
  _FormButtons: {
    padding: '10px 20px',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  _FormButtonPrimary: {
    marginLeft: '10px'
  }
})
