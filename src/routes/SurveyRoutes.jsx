// react-icon-kit
import { ic_dashboard } from 'react-icons-kit/md/ic_dashboard'
//* new views
import RunView from '../views/survey/RunView'

const SurveyRoutes = [
  {
    path: `/run`,
    sidebarName: 'Опрос',
    navbarName: 'Run',
    icon: ic_dashboard,
    component: RunView
  },
  {
    redirect: true,
    path: `/`,
    to: `/admin/dashboard`,
    navbarName: 'Redirect'
  }
]

export default SurveyRoutes
