// react-icon-kit
import { ic_dashboard } from 'react-icons-kit/md/ic_dashboard'
import { u1F356 } from 'react-icons-kit/noto_emoji_regular/u1F356'
import { truck } from 'react-icons-kit/fa/truck'
import { rub } from 'react-icons-kit/fa/rub'
import { tags } from 'react-icons-kit/fa/tags'
import { ic_people } from 'react-icons-kit/md/ic_people'
import { u1F35B } from 'react-icons-kit/noto_emoji_regular/u1F35B'
// views
import DashboardPage from '../views/admin/Dashboard'
// import PaymentView from '../views/PaymentView'
// import OrderView from '../views/OrderView'
// import ProductView from '../views/ProductView'
// import Categories from '../views/CategoryView'
// import CustomerView from '../views/CustomerView'

//* new views
import RunView from '../views/admin/RunView'
import ResearchView from '../views/admin/ResearchView'
import UserView from '../views/admin/UserView'

const prefix = '/admin'

const DashboardRoutes = [
  {
    path: `${prefix}/run`,
    sidebarName: 'Опрос',
    navbarName: 'Run',
    icon: ic_dashboard,
    component: RunView
  },
  {
    path: `${prefix}/dashboard`,
    sidebarName: 'Dashboard',
    navbarName: 'Material Dashboard',
    icon: ic_dashboard,
    component: DashboardPage
  },
  { divider: true },
  {
    path: `${prefix}/researches`,
    sidebarName: 'Исследования',
    navbarName: 'Researches',
    icon: rub,
    component: ResearchView
  },
  // {
  //   path: '/orders',
  //   sidebarName: 'Заказы',
  //   navbarName: 'Orders',
  //   icon: truck,
  //   component: OrderView
  // },
  // { divider: true },
  {
    path: `${prefix}/users`,
    sidebarName: 'Пользователи',
    navbarName: 'Users',
    icon: ic_people,
    component: UserView
  },
  {
    redirect: true,
    path: `${prefix}`,
    to: `${prefix}/dashboard`,
    navbarName: 'Redirect'
  },
  {
    redirect: true,
    path: '/',
    to: `${prefix}/dashboard`,
    navbarName: 'Redirect'
  }
]

export default DashboardRoutes
