import React from 'react'
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect
} from 'react-router-dom'
// Layouts
import SurveyLayout from '../layouts/Survey'
import AdminLayout from '../layouts/Admin'
import Login from '../layouts/Login'
// Private Route (only for authorized users)
import PrivateRoute from './PrivateRoute'

const indexRoutes = [
  { path: '/admin', component: AdminLayout }
  // { path: '/', component: SurveyLayout }
]

class Routes extends React.Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path='/login' component={Login} />
          <Route path='/run' component={SurveyLayout} />
          {indexRoutes.map((prop, key) => {
            return (
              <PrivateRoute
                path={prop.path}
                component={prop.component}
                key={key}
              />
            )
          })}
          <Redirect from='/' to='/admin' />
        </Switch>
      </Router>
    )
  }
}

export default Routes
