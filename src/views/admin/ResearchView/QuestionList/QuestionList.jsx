import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
// Components
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
// @material-ui components
import { Grid, Typography, Button } from '@material-ui/core'
import { List, ListItem } from '@material-ui/core'
import { MuiThemeProvider } from '@material-ui/core/styles'

const QuestionList = ({
  questions,
  url,
  loading,
  editHandler,
  classes,
  theme
}) => {
  return (
    <MuiThemeProvider theme={theme}>
      <Grid className={classes.Wrap}>
        {!loading ? (
          <React.Fragment>
            <Grid className={classes.MB20}>
              <Button
                variant='outlined'
                color='secondary'
                to={`${url}/edit`}
                component={Link}
              >
                Добавить вопрос
              </Button>
            </Grid>
            <List className={classes.MT20}>
              {questions.map(question => {
                return (
                  <ListItem
                    key={question._id}
                    button
                    divider
                    onClick={editHandler(question._id)}
                  >
                    <Typography variant='h6'>{question.title}</Typography>
                  </ListItem>
                )
              })}
            </List>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <Typography variant='h6' className={classes._PageTitle}>
              загрузка...
            </Typography>
            <LinearIndeterminate />
          </React.Fragment>
        )}
      </Grid>
    </MuiThemeProvider>
  )
}

QuestionList.defaultProps = {
  questions: []
}

export default QuestionList
