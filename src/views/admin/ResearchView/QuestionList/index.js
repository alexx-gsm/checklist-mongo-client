import { withRouter } from 'react-router-dom'
import { compose, withState, withHandlers, lifecycle } from 'recompose'
import { connect } from 'react-redux'
// AC
import { getQuestions, clearQuestion } from '../../../../redux/modules/question'
// Material UI
import { withStyles } from '@material-ui/core'
// Styles
import CommonStyles from '../../../../styles/CommonStyles'
import styles from './styles'

import QuestionList from './QuestionList'

export default compose(
  connect(
    ({ surveyStore, questionStore }) => {
      const { survey } = surveyStore
      const { questions, loading, error } = questionStore

      return {
        survey,
        questions: survey._id
          ? questions.filter(q => q.sid === survey._id)
          : [],
        loading,
        error
      }
    },
    { getQuestions, clearQuestion }
  ),
  withRouter,
  withState('isLoadedSurvey', 'setIsLoadedSurvey', false),
  withHandlers({
    onReload: ({}) => () => {},
    editHandler: ({ url, history }) => id => () =>
      history.push(`${url}/edit/${id}`),
    deleteHandler: ({ updateAnchorEl }) => () => () => updateAnchorEl(null)
  }),
  lifecycle({
    componentDidMount() {
      const { _id } = this.props.survey
      if (_id) {
        this.props.getQuestions(_id)
        this.props.setIsLoadedSurvey(true)
      }

      this.props.clearQuestion()
    },
    componentDidUpdate() {
      const { isLoadedSurvey, survey } = this.props

      if (!isLoadedSurvey && survey._id) {
        this.props.getQuestions(survey._id)
        this.props.setIsLoadedSurvey(true)
      }
    }
  }),
  withStyles(
    theme => ({
      ...CommonStyles(theme),
      ...styles(theme)
    }),
    { withTheme: true }
  )
)(QuestionList)
