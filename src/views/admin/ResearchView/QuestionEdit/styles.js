export default theme => ({
  Wrap: { padding: '20px' },
  ExtendedIcon: {
    marginRight: theme.spacing.unit
  },
  FabLabel: {
    whiteSpace: 'nowrap'
  }
})
