import React, { Fragment } from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import classNames from 'classnames'
// Components
// import ToolPanel from '../../../components/ToolPanel'
// import ColorPicker from '../../../components/ColorPicker'
// Material UI
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Fab from '@material-ui/core/Fab'
import Divider from '@material-ui/core/Divider'
import MenuItem from '@material-ui/core/MenuItem'
import Switch from '@material-ui/core/Switch'
import {
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio
} from '@material-ui/core'
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell
} from '@material-ui/core'
// @material-ui/icons
import FiberNew from '@material-ui/icons/FiberNew'
import AddIcon from '@material-ui/icons/Add'
import ClearIcon from '@material-ui/icons/Clear'
import UploadIcon from '@material-ui/icons/CloudUpload'
import DownloadIcon from '@material-ui/icons/CloudDownload'
// icons
import { Icon } from 'react-icons-kit'
import { trashO } from 'react-icons-kit/fa/trashO'
import { download2 } from 'react-icons-kit/icomoon/download2'
// useful tools
import isEmpty from '../../../../helpers/is-empty'
// styles
import { MuiThemeProvider } from '@material-ui/core/styles'

const QuestionEdit = ({
  question,
  answers,
  criterias,
  criteriaGroups,
  error,
  classes,
  onChange,
  onSave,
  onDelete,
  handleAddGroup,
  handleSelectGroup,
  onAddCriteria,
  onDeleteRow,
  theme,
  url,
  showNewCriteria,
  setShowNewCriteria,
  newCriteria,
  setNewCriteria,
  showNewSelectCriteria,
  setShowNewSelectCriteria,
  newSelectCriteria,
  setNewSelectCriteria,
  handleSelectCriteria,
  showNewGroup,
  setShowNewGroup,
  newGroup,
  setNewGroup,
  showNewSelectGroup,
  setShowNewSelectGroup,
  newSelectGroup,
  setNewSelectGroup
}) => {
  return (
    <MuiThemeProvider theme={theme}>
      {/* Form */}
      <Grid className={classes.Wrap}>
        <TextField
          fullWidth
          multiline
          rowsMax='6'
          id='title'
          label='Название'
          value={isEmpty(question.title) ? '' : question.title}
          onChange={onChange('title')}
          margin='dense'
          InputProps={{
            className: classes.Input40px
          }}
          error={Boolean(error.title)}
          helperText={error.title ? error.title : null}
        />

        {answers && (
          <TextField
            select
            fullWidth
            id='type'
            label='Тип'
            value={isEmpty(question.type) ? '' : question.type}
            onChange={onChange('type')}
            margin='normal'
            className={classes.wrapTitle}
            InputProps={{
              className: classes.Title
            }}
            error={Boolean(error.type)}
            helperText={error.type ? error.type : null}
          >
            {answers.map(item => {
              return (
                <MenuItem key={item._id} value={item._id}>
                  <Grid
                    container
                    alignItems='baseline'
                    className={classes.gridType}
                    direction='row'
                    wrap='wrap'
                  >
                    <Typography
                      className={classes.typoSelectInputTitle}
                      variant='h6'
                    >
                      {item.title}
                    </Typography>
                  </Grid>
                </MenuItem>
              )
            })}
          </TextField>
        )}

        {question.type === '2' && (
          <Fragment>
            <Table className={classes.MT20}>
              <TableHead>
                <TableRow className={classes.TableHeadRow}>
                  <TableCell padding='none' style={{ minWidth: '40px' }} />
                  <TableCell width='100%'>
                    <Grid container justify='space-between' alignItems='center'>
                      <Grid item>
                        <Typography
                          variant='caption'
                          className={classes.TableHeadCell}
                        >
                          Критерии
                        </Typography>
                      </Grid>
                    </Grid>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {question.criterias &&
                  question.criterias.map((criteria, index) => (
                    <TableRow key={index}>
                      <TableCell
                        padding='none'
                        className={classNames(
                          classes.removeCell,
                          'icon-remove'
                        )}
                      >
                        <Fab
                          className={classes.removeIcon}
                          onClick={onDeleteRow(index)}
                          size='small'
                        >
                          <ClearIcon />
                        </Fab>
                      </TableCell>
                      <TableCell>
                        <Typography variant='h6'>{criteria.title}</Typography>
                      </TableCell>
                    </TableRow>
                  ))}
              </TableBody>
            </Table>

            {!showNewCriteria &&
              !showNewGroup &&
              !showNewSelectGroup &&
              !showNewSelectCriteria && (
                <Grid
                  container
                  spacing={16}
                  wrap='nowrap'
                  alignItems='center'
                  className={classes.MT20}
                >
                  <Grid item>
                    <Fab
                      onClick={() => setShowNewCriteria(true)}
                      size='medium'
                      color='primary'
                      variant='extended'
                    >
                      <AddIcon className={classes.ExtendedIcon} />
                      Новый
                    </Fab>
                  </Grid>
                  <Grid item>
                    <Fab
                      onClick={() => setShowNewSelectCriteria(true)}
                      size='medium'
                      color='primary'
                      variant='extended'
                      classes={{
                        label: classes.FabLabel
                      }}
                    >
                      <AddIcon className={classes.ExtendedIcon} />
                      Из списка
                    </Fab>
                  </Grid>
                  <Grid item container spacing={8} justify='flex-end'>
                    <Grid item>
                      <Fab
                        className={classes.addButton}
                        onClick={() => setShowNewSelectGroup(true)}
                        size='medium'
                        color='primary'
                        disabled={isEmpty(criteriaGroups)}
                      >
                        <DownloadIcon />
                      </Fab>
                    </Grid>
                    <Grid item>
                      <Fab
                        className={classes.addButton}
                        onClick={() => setShowNewGroup(true)}
                        size='medium'
                        color='primary'
                      >
                        <UploadIcon />
                      </Fab>
                    </Grid>
                  </Grid>
                </Grid>
              )}

            {showNewCriteria && (
              <Grid
                container
                className={classes.MT20}
                direction='column'
                spacing={8}
              >
                <Grid item>
                  <TextField
                    fullWidth
                    multiline
                    rows='2'
                    rowsMax='6'
                    id='criteria'
                    label='Новый критерий'
                    value={newCriteria}
                    onChange={e => setNewCriteria(e.target.value)}
                    margin='dense'
                    variant='outlined'
                    InputLabelProps={{
                      shrink: true
                    }}
                  />
                </Grid>
                <Grid item>
                  <Button
                    variant='contained'
                    fullWidth
                    color='primary'
                    onClick={onAddCriteria}
                    disabled={isEmpty(newCriteria)}
                  >
                    Добавить критерий
                  </Button>
                </Grid>
                <Grid item>
                  <Button
                    variant='outlined'
                    fullWidth
                    onClick={() => setShowNewCriteria(false)}
                  >
                    Закрыть
                  </Button>
                </Grid>
              </Grid>
            )}
            {showNewSelectCriteria && (
              <Grid
                container
                className={classes.MT20}
                direction='column'
                spacing={8}
              >
                <Grid item>
                  <TextField
                    select
                    fullWidth
                    id='select-criteria'
                    label='Критерии'
                    value={newSelectCriteria}
                    onChange={e => setNewSelectCriteria(e.target.value)}
                    margin='dense'
                  >
                    {criterias.map(item => {
                      return (
                        <MenuItem key={item._id} value={item._id}>
                          <Grid
                            container
                            alignItems='baseline'
                            className={classes.gridType}
                            direction='row'
                            wrap='wrap'
                          >
                            <Typography
                              className={classes.typoSelectInputTitle}
                              variant='h6'
                            >
                              {item.title}
                            </Typography>
                          </Grid>
                        </MenuItem>
                      )
                    })}
                  </TextField>
                </Grid>
                <Grid item>
                  <Button
                    variant='contained'
                    fullWidth
                    color='primary'
                    onClick={handleSelectCriteria}
                    disabled={isEmpty(newSelectCriteria)}
                  >
                    Выбрать критерий
                  </Button>
                </Grid>
                <Grid item>
                  <Button
                    variant='outlined'
                    fullWidth
                    onClick={() => setShowNewSelectCriteria(false)}
                  >
                    Закрыть
                  </Button>
                </Grid>
              </Grid>
            )}
            {showNewGroup && (
              <Grid
                container
                className={classes.MT20}
                direction='column'
                spacing={8}
              >
                <Grid item>
                  <TextField
                    fullWidth
                    multiline
                    id='group'
                    label='Новая группа'
                    value={newGroup}
                    onChange={e => setNewGroup(e.target.value)}
                    margin='dense'
                    variant='outlined'
                    InputLabelProps={{
                      shrink: true
                    }}
                  />
                </Grid>
                <Grid item>
                  <Button
                    variant='contained'
                    fullWidth
                    color='primary'
                    onClick={handleAddGroup}
                    disabled={isEmpty(newGroup)}
                  >
                    Сохранить группу
                  </Button>
                </Grid>
                <Grid item>
                  <Button
                    variant='outlined'
                    fullWidth
                    onClick={() => setShowNewGroup(false)}
                  >
                    Закрыть
                  </Button>
                </Grid>
              </Grid>
            )}
            {showNewSelectGroup && (
              <Grid
                container
                className={classes.MT20}
                direction='column'
                spacing={8}
              >
                <Grid item>
                  <TextField
                    select
                    fullWidth
                    id='select-group'
                    label='Группы критериев'
                    value={newSelectGroup}
                    onChange={e => setNewSelectGroup(e.target.value)}
                    margin='dense'
                  >
                    {Object.keys(criteriaGroups).map(item => {
                      return (
                        <MenuItem key={item} value={item}>
                          <Grid
                            container
                            alignItems='baseline'
                            className={classes.gridType}
                            direction='row'
                            wrap='wrap'
                          >
                            <Typography
                              className={classes.typoSelectInputTitle}
                              variant='h6'
                            >
                              {item}
                            </Typography>
                          </Grid>
                        </MenuItem>
                      )
                    })}
                  </TextField>
                </Grid>
                <Grid item>
                  <Button
                    variant='contained'
                    fullWidth
                    color='primary'
                    onClick={handleSelectGroup}
                    disabled={isEmpty(newSelectGroup)}
                  >
                    Загрузить группу
                  </Button>
                </Grid>
                <Grid item>
                  <Button
                    variant='outlined'
                    fullWidth
                    onClick={() => setShowNewSelectGroup(false)}
                  >
                    Закрыть
                  </Button>
                </Grid>
              </Grid>
            )}
          </Fragment>
        )}
      </Grid>

      <Divider />

      <Grid container className={classes.P20} justify='space-between'>
        <Grid item>
          <Fab
            size='small'
            aria-label='Delete'
            color='secondary'
            className={classes.CardButtonDelete}
            onClick={onDelete}
          >
            <Icon size={18} icon={trashO} />
          </Fab>
        </Grid>
        <Grid item>
          <Button
            to={url}
            component={Link}
            aria-label='Back'
            variant='contained'
          >
            Отмена
          </Button>
          <Button
            onClick={onSave}
            aria-label='Save'
            variant='contained'
            color='primary'
            className={classes._FormButtonPrimary}
          >
            Сохранить вопрос
          </Button>
        </Grid>
      </Grid>
    </MuiThemeProvider>
  )
}

QuestionEdit.defaultProps = {
  question: {},
  answer: {},
  criterias: []
}

export default QuestionEdit
