import { withRouter } from 'react-router-dom'
import {
  compose,
  withProps,
  withState,
  withHandlers,
  lifecycle
} from 'recompose'
import { connect } from 'react-redux'
// AC
import {
  getQuestionById,
  storeQuestion,
  saveQuestion
} from '../../../../redux/modules/question'
import { saveCriteria, getCriterias } from '../../../../redux/modules/criteria'
// import { getAnswers } from '../../../redux/modules/answer'
import { storeSurvey } from '../../../../redux/modules/survey'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CommonStyles from '../../../../styles/CommonStyles'
// useful tools
import isEmpty from '../../../../helpers/is-empty'

import QuestionEdit from './QuestionEdit'

export default compose(
  connect(
    ({ researchStore, surveyStore, questionStore, criteriaStore }) => {
      const { research } = researchStore
      const { survey } = surveyStore
      const { question, answers, error, loading } = questionStore
      const { criterias } = criteriaStore

      return {
        research,
        survey,
        question,
        answers,
        criterias,
        criteriaGroups: survey.criteriaGroups ? survey.criteriaGroups : {},
        error,
        loading
      }
    },
    {
      getQuestionById,
      storeQuestion,
      saveQuestion,
      storeSurvey,
      saveCriteria,
      getCriterias
    }
  ),
  withRouter,
  withState('uploadedFile', 'setUploadedFile', null),
  withState('showNewCriteria', 'setShowNewCriteria', false),
  withState('newCriteria', 'setNewCriteria', ''),
  withState('showNewSelectCriteria', 'setShowNewSelectCriteria', false),
  withState('newSelectCriteria', 'setNewSelectCriteria', ''),
  withState('showNewGroup', 'setShowNewGroup', false),
  withState('newGroup', 'setNewGroup', ''),
  withState('showNewSelectGroup', 'setShowNewSelectGroup', false),
  withState('newSelectGroup', 'setNewSelectGroup', ''),
  withState('showNewSelectCriteria', 'setShowNewSelectCriteria', false),
  withState('newSelectCriteria', 'setNewSelectCriteria', ''),
  withHandlers({
    onReload: ({ getQuestionById, match }) => () =>
      getQuestionById(match.params.id),
    onChange: ({ question, storeQuestion }) => index => event => {
      if (index === 'checkBox') {
        storeQuestion({
          ...question,
          [event.target.name]: event.target.checked
        })
      } else if (index === 'file') {
        storeQuestion({
          ...question,
          file: event.target.files[0]
        })
      } else {
        storeQuestion({
          ...question,
          [index]: event.target.value
        })
      }
    },
    onChangeItem: ({ question, storeQuestion }) => index => event => {
      storeQuestion({
        ...question,
        criterias: [
          ...question.criterias.slice(0, index),
          {
            ...question.criterias[index],
            [event.target.name]: event.target.value
          },
          ...question.criterias.slice(index + 1)
        ]
      })
    },
    onAddCriteria: ({
      survey,
      question,
      newCriteria,
      setNewCriteria,
      setShowNewCriteria,
      storeQuestion,
      saveCriteria
    }) => () => {
      saveCriteria({ sid: survey._id, title: newCriteria }, criteria => {
        storeQuestion({
          ...question,
          criterias: [...question.criterias, criteria]
        })
      })
      setNewCriteria('')
      setShowNewCriteria(false)
    },
    handleSelectCriteria: ({
      question,
      criterias,
      newSelectCriteria,
      storeQuestion,
      setNewSelectCriteria,
      setShowNewSelectCriteria
    }) => () => {
      storeQuestion({
        ...question,
        criterias: [
          ...question.criterias,
          criterias.find(c => c._id === newSelectCriteria)
        ]
      })
      setNewSelectCriteria('')
      setShowNewSelectCriteria(false)
    },
    handleAddGroup: ({
      question,
      survey,
      criteriaGroups,
      newGroup,
      setNewGroup,
      setShowNewGroup,
      storeSurvey
    }) => () => {
      if (!isEmpty(newGroup)) {
        storeSurvey({
          ...survey,
          criteriaGroups: {
            ...criteriaGroups,
            [newGroup]: question.criterias
          }
        })
        setNewGroup('')
        setShowNewGroup(false)
      }
    },
    handleSelectGroup: ({
      question,
      newSelectGroup,
      setNewSelectGroup,
      setShowNewSelectGroup,
      criteriaGroups,
      storeQuestion
    }) => () => {
      storeQuestion({
        ...question,
        criterias: criteriaGroups[newSelectGroup]
      })
      setNewSelectGroup('')
      setShowNewSelectGroup(false)
    },
    onDeleteRow: ({ question, storeQuestion }) => index => () => {
      storeQuestion({
        ...question,
        criterias: [
          ...question.criterias.slice(0, index),
          ...question.criterias.slice(index + 1)
        ]
      })
    },
    onSave: ({
      survey,
      question,
      filteredCriterias,
      saveQuestion,
      history,
      basePath,
      url
    }) => () => {
      saveQuestion(
        {
          ...question,
          sid: question.sid ? question.sid : survey._id
        },
        () => history.push(url)
      )
    },
    onDelete: () => () => {},
    onUpload: ({ question, uploadedCriterias, uploadCriterias }) => () => {
      // if (!isEmpty(uploadedCriterias)) {
      //   clearUploadedCriterias()
      // } else {
      //   uploadCriterias(question)
      // }
    }
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params
      if (!isEmpty(id)) {
        this.props.getQuestionById(id)
      }
      this.props.getCriterias(this.props.research._id)
    },
    componentWillUnmount() {
      this.props.storeQuestion({})
    }
  }),
  withStyles(theme => ({ ...CommonStyles(theme), ...styles(theme) }), {
    theme: true
  })
)(QuestionEdit)
