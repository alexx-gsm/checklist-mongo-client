import {
  compose,
  withProps,
  withState,
  withHandlers,
  lifecycle
} from 'recompose'
import { withRouter } from 'react-router-dom'
// styles & theme
import styles from './styles'
import theme from './theme'
import tabs from './tabs'
// @material ui
import { withStyles } from '@material-ui/core'
// Component
import SurveyTabs from './SurveyTabs'

export default compose(
  withRouter,
  withState('tabIndex', 'setTabIndex', 0),
  withProps({
    theme,
    tabs
  }),
  withHandlers({
    onTabChange: ({ setTabIndex, history, url }) => (event, value) => {
      setTabIndex(value)
      history.push(`${url}${tabs[value].link}`)
    },
    onChangeIndex: ({ setTabIndex, url, history }) => index => {
      setTabIndex(index)
      history.push(`${url}${tabs[index].link}`)
    }
  }),
  lifecycle({
    componentDidMount() {
      const { pathname } = this.props.location
      this.props.tabs.map((tab, index) => {
        if (pathname.includes(tab.link)) {
          this.props.setTabIndex(index)
        }
      })
    }
  }),
  withStyles(styles, {
    withTheme: true
  })
)(SurveyTabs)
