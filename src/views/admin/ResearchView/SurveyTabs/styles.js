export default theme => ({
  CardTabs: {
    marginTop: '20px',
    marginBottom: '40px',
    background: theme.palette.grey[50],
    [theme.breakpoints.down('xs')]: {
      marginTop: '20px'
    }
  },
  CardTabsHeader: {
    padding: 0,
    [theme.breakpoints.down('xs')]: {
      borderRadius: 0,
      margin: '0'
    }
  },
  AppBarPrimary: {
    // backgroundColor: 'transparent'
  },
  TabsRoot: {},
  TabLabel: {
    fontWeight: theme.typography.fontWeightMedium
  }
})
