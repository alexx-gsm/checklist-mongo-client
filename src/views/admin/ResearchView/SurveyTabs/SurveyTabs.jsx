import React, { lazy, Suspense } from 'react'
import { Route } from 'react-router-dom'
import classNames from 'classnames'
import SwipeableViews from 'react-swipeable-views'
import { Paper, Typography } from '@material-ui/core'
import { AppBar, Tabs, Tab } from '@material-ui/core'
// components
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
// styles
import { MuiThemeProvider } from '@material-ui/core/styles'
// TAB Components
const QuestionList = lazy(() => import('../QuestionList'))
const QuestionEdit = lazy(() => import('../QuestionEdit'))
const RespondentList = lazy(() => import('../RespondentList'))
const RespondentEdit = lazy(() => import('../RespondentEdit'))

const getTab = (label, classes) => (
  <Tab
    key={label}
    label={label}
    classes={{
      label: classes.TabLabel
    }}
  />
)

const TabsView = ({
  tabs,
  tabIndex,
  onTabChange,
  onChangeIndex,
  classes,
  theme,
  url
}) => {
  return (
    <MuiThemeProvider theme={theme}>
      <Paper className={classes.CardTabs} square>
        <Paper className={classes.CardTabsHeader} square>
          <AppBar
            position='static'
            classes={{ colorPrimary: classes.AppBarPrimary }}
          >
            <Tabs
              value={tabIndex}
              onChange={onTabChange}
              classes={{
                indicator: classes.tabsIndicator
              }}
              variant='fullWidth'
            >
              {tabs.map(tab => getTab(tab.label, classes))}
            </Tabs>
          </AppBar>
        </Paper>
        <SwipeableViews
          axis={'x'}
          index={tabIndex}
          onChangeIndex={onChangeIndex}
        >
          <Suspense fallback={<LinearIndeterminate />}>
            <Route
              exact
              path={`${url}`}
              render={() => <QuestionList theme={theme} url={url} />}
            />
            <Route
              path={`${url}/edit/:id?`}
              render={() => <QuestionEdit theme={theme} url={url} />}
            />
          </Suspense>
          <Suspense fallback={<LinearIndeterminate />}>
            <Route
              exact
              path={`${url}/respondents`}
              render={() => (
                <RespondentList theme={theme} url={`${url}/respondents`} />
              )}
            />
            <Route
              path={`${url}/respondents/edit/:id?`}
              render={() => (
                <RespondentEdit theme={theme} url={`${url}/respondents`} />
              )}
            />
          </Suspense>
        </SwipeableViews>
      </Paper>
    </MuiThemeProvider>
  )
}

export default TabsView
