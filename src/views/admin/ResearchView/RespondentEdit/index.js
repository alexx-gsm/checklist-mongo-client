import { compose, withHandlers, lifecycle, withState } from 'recompose'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
// AC
import {
  getRespondents,
  storeRespondent,
  getOneRespondent,
  saveRespondent
} from '../../../../redux/modules/respondent'
import { saveResult } from '../../../../redux/modules/result'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CommonStyles from '../../../../styles/CommonStyles'
// Useful tools
import isEmpty from '../../../../helpers/is-empty'
import uuidv1 from 'uuid/v1'

import RespondentEdit from './RespondentEdit'

export default compose(
  connect(
    ({ surveyStore, respondentStore }) => {
      const { survey } = surveyStore
      const { respondent, loading, error } = respondentStore

      return {
        survey,
        respondent,
        error,
        loading
      }
    },
    {
      getRespondents,
      storeRespondent,
      getOneRespondent,
      saveRespondent,
      saveResult
    }
  ),
  withRouter,
  withState('uploadStage', 'setUploadStage', 0),
  withState('csvHeader', 'setCsvHeader', []),
  withState('csvData', 'setCsvData', []),
  withState('rCode', 'setRCode', ''),
  withState('rName', 'setRName', ''),
  withHandlers({
    onChange: ({ respondent, storeRespondent }) => index => event => {
      storeRespondent({
        ...respondent,
        [index]: event.target.value
      })
    },
    onSave: ({ survey, respondent, saveRespondent, history, url }) => () => {
      saveRespondent(
        {
          ...respondent,
          sid: respondent.sid ? respondent.sid : survey._id
        },
        () => history.push(url)
      )
    },
    onGenerateLink: ({
      survey,
      respondent,
      saveRespondent,
      saveResult
    }) => () => {
      saveRespondent(
        {
          ...respondent,
          sid: respondent.sid ? respondent.sid : survey._id,
          link: uuidv1()
        },
        respondent =>
          saveResult({
            surveyId: respondent.sid,
            respondentId: respondent._id,
            link: respondent.link
          })
      )
    }
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params
      if (id) this.props.getOneRespondent(id)
    },
    componentWillUnmount() {
      this.props.storeRespondent({})
    }
  }),
  withStyles(theme => ({ ...CommonStyles(theme), ...styles(theme) }), {
    theme: true
  })
)(RespondentEdit)
