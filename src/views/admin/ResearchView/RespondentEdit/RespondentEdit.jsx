import React, { Fragment } from 'react'
import CSVReader from 'react-csv-reader'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
// Components
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
// Material UI
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import MenuItem from '@material-ui/core/MenuItem'
import { List, ListItem } from '@material-ui/core'
import { MuiThemeProvider } from '@material-ui/core/styles'
// useful tool
import isEmpty from '../../../../helpers/is-empty'

const RespondentEdit = ({
  respondent,
  classes,
  onChange,
  onSave,
  onGenerateLink,
  error,
  loading,
  url,
  theme
}) => {
  return (
    <MuiThemeProvider theme={theme}>
      <Grid className={classes.Wrap}>
        {!loading ? (
          <React.Fragment>
            <TextField
              fullWidth
              id='name'
              label='ФИО'
              value={isEmpty(respondent.name) ? '' : respondent.name}
              onChange={onChange('name')}
              margin='dense'
              InputProps={{
                className: classes.Input40px
              }}
              error={Boolean(error.name)}
              helperText={error.name ? error.name : null}
            />

            <TextField
              fullWidth
              id='name'
              label='Код'
              value={isEmpty(respondent.code) ? '' : respondent.code}
              onChange={onChange('code')}
              margin='dense'
              InputProps={{
                className: classes.Input40px
              }}
              error={Boolean(error.code)}
              helperText={error.code ? error.code : null}
            />

            {respondent.link ? (
              <Grid
                container
                spacing={8}
                alignItems='center'
                className={classes.MT20}
              >
                <Grid item>
                  <Button
                    href={`http://run.vk-da.ru/survey/${respondent.link}`}
                    variant='contained'
                    color='primary'
                    component='a'
                  >
                    Ссылка
                  </Button>
                </Grid>
                <Grid item>
                  <Typography variant='caption'>{respondent.link}</Typography>
                </Grid>
              </Grid>
            ) : (
              <Grid className={classes.MT20}>
                <Button
                  variant='outlined'
                  color='primary'
                  onClick={onGenerateLink}
                  disabled={
                    isEmpty(respondent.name) || isEmpty(respondent.code)
                  }
                >
                  Сгененировать ссылку
                </Button>
              </Grid>
            )}

            <Grid container className={classes.MT40} justify='space-between'>
              <Grid item>
                <Button variant='outlined' to={`${url}`} component={Link}>
                  К списку
                </Button>
              </Grid>
              <Grid item>
                <Button
                  to={url}
                  component={Link}
                  aria-label='Back'
                  variant='contained'
                >
                  Отмена
                </Button>
                <Button
                  onClick={onSave}
                  aria-label='Save'
                  variant='contained'
                  className={classes._FormButtonPrimary}
                  color='primary'
                >
                  Сохранить
                </Button>
              </Grid>
            </Grid>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <Typography variant='h6' className={classes._PageTitle}>
              загрузка...
            </Typography>
            <LinearIndeterminate />
          </React.Fragment>
        )}
      </Grid>
    </MuiThemeProvider>
  )
}

export default RespondentEdit
