export default theme => ({
  Wrap: {
    padding: '20px'
  },
  Upload: {
    display: 'none'
  },
  UploadButton: {
    margin: theme.spacing.unit
  },
  UploadList: {
    maxHeight: '60vh',
    overflow: 'auto'
  }
})
