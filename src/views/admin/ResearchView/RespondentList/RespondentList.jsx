import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import CSVReader from 'react-csv-reader'
// Components
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
// @material-ui components
import { Paper, Grid, Typography, Button } from '@material-ui/core'
import { TextField, List, ListItem, MenuItem } from '@material-ui/core'
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell,
  TableFooter
} from '@material-ui/core'
import { MuiThemeProvider } from '@material-ui/core/styles'
import IconLink from '@material-ui/icons/Link'
import IconDelete from '@material-ui/icons/Delete'
// useful tools
import isEmpty from '../../../../helpers/is-empty'

const RespondentList = ({
  respondents,
  loading,
  editHandler,
  uploadStage,
  handleForce,
  handleDarkSideForce,
  csvHeader,
  csvData,
  rCode,
  rName,
  setRCode,
  setRName,
  handleUpload,
  handleReset,
  onSave,
  deleteHandler,
  onReload,
  onSearch,
  url,
  error,
  classes,
  theme
}) => {
  return (
    <MuiThemeProvider theme={theme}>
      <Grid className={classes.Wrap}>
        {!loading ? (
          <React.Fragment>
            {uploadStage === 0 && (
              <React.Fragment>
                <Grid
                  className={classes.MB20}
                  container
                  spacing={8}
                  justify='space-between'
                  alignItems='center'
                >
                  <Grid item>
                    <Button
                      variant='outlined'
                      color='secondary'
                      to={`${url}/edit`}
                      component={Link}
                    >
                      Добавить
                    </Button>
                  </Grid>
                  <Grid item>
                    <CSVReader
                      cssClass='csv-reader-input'
                      onFileLoaded={handleForce}
                      onError={handleDarkSideForce}
                      inputId='upload'
                      inputStyle={{ display: 'none' }}
                    />

                    <label htmlFor='upload'>
                      <Button
                        variant='contained'
                        component='span'
                        className={classes.UploadButton}
                      >
                        Выбрать *.csv файл
                      </Button>
                    </label>
                  </Grid>
                </Grid>

                <List className={classes.MT20}>
                  {respondents.map(respondent => {
                    return (
                      <ListItem
                        key={respondent._id}
                        button
                        divider
                        onClick={editHandler(respondent._id)}
                      >
                        <Grid container wrap='nowrap' alignItems='center'>
                          <Grid item container spacing={8} alignItems='center'>
                            <Grid item>
                              <Typography variant='h6'>
                                {respondent.code}
                              </Typography>
                            </Grid>
                            <Grid item>
                              <Typography variant='caption'>
                                {respondent.name}
                              </Typography>
                            </Grid>
                          </Grid>
                          <Grid item>{respondent.link && <IconLink />}</Grid>
                        </Grid>
                      </ListItem>
                    )
                  })}
                </List>
              </React.Fragment>
            )}
            {uploadStage > 0 && (
              <Grid container spacing={8} justify='space-between'>
                <Grid item>
                  {uploadStage === 1 && !isEmpty(rCode) && !isEmpty(rName) && (
                    <Button
                      onClick={handleUpload}
                      variant='outlined'
                      color='primary'
                    >
                      Считать данные
                    </Button>
                  )}
                  {uploadStage === 2 && (
                    <Button
                      onClick={onSave}
                      aria-label='Save'
                      variant='contained'
                      color='primary'
                    >
                      Сохранить данные
                    </Button>
                  )}
                </Grid>
                <Grid item>
                  <Button
                    onClick={handleReset}
                    variant='outlined'
                    color='secondary'
                  >
                    Сброс
                  </Button>
                </Grid>
              </Grid>
            )}

            {uploadStage === 1 && (
              <Fragment>
                <TextField
                  select
                  fullWidth
                  id='rCode'
                  label='Код'
                  value={rCode}
                  onChange={e => setRCode(e.target.value)}
                  margin='normal'
                  className={classes.wrapTitle}
                  InputProps={{
                    className: classes.Title
                  }}
                  error={Boolean(error.answerId)}
                  helperText={error.answerId ? error.answerId : null}
                >
                  {csvHeader.map((item, index) => {
                    return (
                      <MenuItem key={index} value={index}>
                        <Grid
                          container
                          justify='space-between'
                          alignItems='baseline'
                          className={classes.gridType}
                          direction='row'
                          wrap='wrap'
                        >
                          <Typography
                            className={classes.typoSelectInputTitle}
                            variant='h6'
                          >
                            {item}
                          </Typography>
                          <Typography variant='caption'>
                            {csvData ? csvData[1][index] : ''}
                          </Typography>
                        </Grid>
                      </MenuItem>
                    )
                  })}
                </TextField>
                <TextField
                  select
                  fullWidth
                  id='rCode'
                  label='ФИО'
                  value={rName}
                  onChange={e => setRName(e.target.value)}
                  margin='normal'
                  className={classes.wrapTitle}
                  InputProps={{
                    className: classes.Title
                  }}
                  error={Boolean(error.answerId)}
                  helperText={error.answerId ? error.answerId : null}
                >
                  {csvHeader.map((item, index) => {
                    return (
                      <MenuItem key={index} value={index} divider>
                        <Grid
                          container
                          alignItems='baseline'
                          justify='space-between'
                          className={classes.gridType}
                          direction='row'
                          wrap='wrap'
                        >
                          <Typography
                            className={classes.typoSelectInputTitle}
                            variant='h6'
                          >
                            {item}
                          </Typography>
                          <Typography variant='caption'>
                            {csvData ? csvData[1][index] : ''}
                          </Typography>
                        </Grid>
                      </MenuItem>
                    )
                  })}
                </TextField>
              </Fragment>
            )}

            {uploadStage === 2 && (
              <List className={classes.MT20}>
                {csvData.map(respondent => {
                  return (
                    <ListItem
                      key={respondent.code}
                      button
                      divider
                      // onClick={editHandler(respondent._id)}
                    >
                      <Typography variant='h6'>{respondent.name}</Typography>
                    </ListItem>
                  )
                })}
              </List>
            )}
          </React.Fragment>
        ) : (
          <React.Fragment>
            <Typography variant='h6' className={classes._PageTitle}>
              загрузка...
            </Typography>
            <LinearIndeterminate />
          </React.Fragment>
        )}
      </Grid>
    </MuiThemeProvider>
    // <MuiThemeProvider theme={theme}>

    //   <Paper className={classes.PaperTable}>
    //     <Grid container className={classes.gridTable}>
    //       <Table>
    //         <TableHead className={classes.TableHead}>
    //           <TableRow>
    //             <TableCell padding='checkbox' />
    //             <TableCell className={classes.headCellData} padding='checkbox'>
    //               <Typography variant='caption'>ФИО</Typography>
    //             </TableCell>
    //             <TableCell />
    //           </TableRow>
    //         </TableHead>
    //         <TableBody>
    //           {Object.keys(respondents).map(key => {
    //             const respondent = respondents[key]
    //             return (
    //               <TableRow key={key} className={classes.TableRow}>
    //                 <TableCell padding='checkbox'>
    //                   <Typography variant='h5' className={classes.Key}>
    //                     {key}
    //                   </Typography>
    //                 </TableCell>
    //                 <TableCell
    //                   padding='checkbox'
    //                   className={classes.CellBordered}
    //                 >
    //                   <Grid container>
    //                     <Typography variant='h6' className={classes.Title}>
    //                       {respondent.fio}
    //                     </Typography>
    //                     <Grid item container direction='row' spacing={16}>
    //                       <Grid item>
    //                         <Typography
    //                           variant='caption'
    //                           className={classes.typoInn}
    //                         >
    //                           {!isEmpty(companies)
    //                             ? companies[respondent.companyId].title
    //                             : ''}
    //                         </Typography>
    //                       </Grid>
    //                       <Grid item>
    //                         <Typography variant='caption'>
    //                           отдел:{' '}
    //                           {!isEmpty(departments)
    //                             ? departments[respondent.departmentId].title
    //                             : ''}
    //                         </Typography>
    //                       </Grid>
    //                     </Grid>
    //                   </Grid>
    //                 </TableCell>
    //                 <TableCell padding='none'>
    //                   <DropdownMenu
    //                     anchorEl={anchorEl}
    //                     updateAnchorEl={updateAnchorEl}
    //                     menuId={key}
    //                     menuList={[
    //                       {
    //                         icon: <IconEdit />,
    //                         label: 'Edit',
    //                         handler: editHandler(key)
    //                       },
    //                       {
    //                         icon: <IconDelete />,
    //                         label: 'Delete',
    //                         handler: deleteHandler(key)
    //                       }
    //                     ]}
    //                   />
    //                 </TableCell>
    //               </TableRow>
    //             )
    //           })}
    //         </TableBody>
    //       </Table>
    //     </Grid>
    //   </Paper>
    // </MuiThemeProvider>
  )
}

RespondentList.defaultProps = {
  respondents: []
}

export default RespondentList
