import { withRouter } from 'react-router-dom'
import { compose, withState, withHandlers, lifecycle } from 'recompose'
import { connect } from 'react-redux'
// AC
import {
  getRespondents,
  uploadRespondents
} from '../../../../redux/modules/respondent'
// Material UI
import { withStyles } from '@material-ui/core'
// Styles
import CommonStyles from '../../../../styles/CommonStyles'
import styles from './styles'

import RespondentList from './RespondentList'

export default compose(
  connect(
    ({ surveyStore, respondentStore }) => {
      const { survey } = surveyStore
      const { respondents, loading, error } = respondentStore

      return {
        survey,
        respondents: survey._id
          ? respondents.filter(q => q.sid === survey._id)
          : [],
        loading,
        error
      }
    },
    { getRespondents, uploadRespondents }
  ),
  withRouter,
  withState('isLoadedSurvey', 'setIsLoadedSurvey', false),
  withState('uploadStage', 'setUploadStage', 0),
  withState('csvHeader', 'setCsvHeader', []),
  withState('csvData', 'setCsvData', []),
  withState('rCode', 'setRCode', ''),
  withState('rName', 'setRName', ''),
  withHandlers({
    onReload: ({}) => () => {},
    editHandler: ({ url, history }) => id => () =>
      history.push(`${url}/edit/${id}`),
    handleForce: ({ setUploadStage, setCsvHeader, setCsvData }) => csv => {
      if (csv) {
        const [header, ...data] = csv
        setCsvHeader(header)
        setCsvData(data)
        setUploadStage(1)
      }
    },
    handleUpload: ({
      survey,
      csvData,
      rCode,
      rName,
      setCsvData,
      setUploadStage
    }) => () => {
      if ((rCode, rName)) {
        const newData = csvData.reduce((acc, row) => {
          return row[rCode]
            ? [
                ...acc,
                {
                  code: row[rCode],
                  name: row[rName],
                  sid: survey._id
                }
              ]
            : acc
        }, [])
        setCsvData(newData)
        setUploadStage(2)
      }
    },
    handleReset: props => () => {
      props.setUploadStage(0)
      props.setRCode('')
      props.setRName('')
      props.setCsvHeader([])
      props.setCsvData([])
    },
    onSave: ({ survey, csvData, uploadRespondents, setUploadStage }) => () => {
      uploadRespondents(csvData, survey._id)

      setUploadStage(0)
    }
  }),
  lifecycle({
    componentDidMount() {
      const { _id } = this.props.survey
      if (_id) {
        this.props.getRespondents(_id)
      }
    }
  }),
  withStyles(
    theme => ({
      ...CommonStyles(theme),
      ...styles(theme)
    }),
    { withTheme: true }
  )
)(RespondentList)
