import { withRouter } from 'react-router-dom'
import { compose, withState, withHandlers, lifecycle } from 'recompose'
import { connect } from 'react-redux'
// AC
import {
  getResearches,
  storeResearch
} from '../../../../redux/modules/research'
// Material UI
import { withStyles } from '@material-ui/core'
// Styles
import CommonStyles from '../../../../styles/CommonStyles'
import styles from './styles'

import ResearchList from './ResearchList'

export default compose(
  connect(
    ({ researchStore, auth }) => {
      const { researches, loading, error } = researchStore
      const { user } = auth

      return {
        researches,
        user,
        loading,
        error
      }
    },
    { getResearches, storeResearch }
  ),
  withRouter,
  withHandlers({
    onReload: ({ getUsers }) => () => getUsers(),
    editHandler: ({ basePath, history }) => id => () =>
      history.push(`${basePath}/edit/${id}`),
    deleteHandler: ({ updateAnchorEl }) => () => () => updateAnchorEl(null)
  }),
  lifecycle({
    componentDidMount() {
      this.props.getResearches()
      this.props.storeResearch({})
    }
  }),
  withStyles(
    theme => ({
      ...CommonStyles(theme),
      ...styles(theme)
    }),
    { withTheme: true }
  )
)(ResearchList)
