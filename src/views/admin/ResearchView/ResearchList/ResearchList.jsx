import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
// @material-ui components
import { Grid, Typography, Button } from "@material-ui/core";
import { List, ListItem, ListItemText } from "@material-ui/core";
import LinearIndeterminate from "../../../../components/LinearIndeterminate";
import { MuiThemeProvider } from "@material-ui/core/styles";
import TopPanel from "../../../../components/TopPanel";

const ResearchList = ({
  researches,
  url,
  basePath,
  loading,
  editHandler,
  classes,
  theme
}) => {
  console.log("url", url);
  console.log("basePath", basePath);
  return (
    <MuiThemeProvider theme={theme}>
      <Grid className={classes._Wrap}>
        {!loading ? (
          <React.Fragment>
            <TopPanel title='Исследования' />
            <List className={classes.MB20}>
              {researches.map(research => {
                return (
                  <ListItem
                    key={research._id}
                    button
                    divider
                    onClick={editHandler(research._id)}
                  >
                    <Typography variant='h6'>{research.title}</Typography>
                  </ListItem>
                );
              })}
            </List>
            <Grid className={classes.MT20}>
              <Button
                variant='outlined'
                color='secondary'
                to={`${basePath}/edit`}
                component={Link}
              >
                Добавить
              </Button>
            </Grid>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <Typography variant='h6' className={classes._PageTitle}>
              загрузка...
            </Typography>
            <LinearIndeterminate />
          </React.Fragment>
        )}
      </Grid>
    </MuiThemeProvider>
  );
};

ResearchList.defaultProps = {
  researches: []
};

ResearchList.propTypes = {
  researches: PropTypes.arrayOf(PropTypes.object),
  loading: PropTypes.bool,
  classes: PropTypes.object
};

export default ResearchList;
