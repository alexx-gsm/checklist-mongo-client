import { compose, withHandlers, lifecycle, withState } from "recompose";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
// AC
import {
  getResearch,
  storeResearch,
  saveResearch
} from "../../../../redux/modules/research";
import { getSurveys, storeSurvey } from "../../../../redux/modules/survey";
// Styles
import { withStyles } from "@material-ui/core/styles";
import styles from "./styles";
import CommonStyles from "../../../../styles/CommonStyles";
// Useful tools
import isEmpty from "../../../../helpers/is-empty";

import ResearchEdit from "./ResearchEdit";

export default compose(
  connect(
    ({ auth, researchStore, surveyStore }) => {
      const { research, loading, error } = researchStore;
      const { surveys } = surveyStore;
      const { user } = auth;
      return {
        research,
        surveys: research._id
          ? surveys.filter(s => s.rid === research._id)
          : [],
        user,
        error,
        loading: loading && surveyStore.loading
      };
    },
    { getResearch, storeResearch, saveResearch, getSurveys, storeSurvey }
  ),
  withRouter,
  withState("isLoadedResearch", "setIsLoadedResearch", false),
  withHandlers({
    onReload: ({}) => () => {},
    onChange: ({ research, storeResearch }) => index => event => {
      storeResearch({
        ...research,
        [index]: event.target.value
      });
    },
    handleClick: ({ research, basePath, history }) => id => () =>
      history.push(`${basePath}/${research._id}/${id}`),
    handleAddClick: ({ history, basePath, research }) => () => {
      history.push(`${basePath}/${research._id}/`);
    },
    onSave: ({ research, user, saveResearch, basePath, history }) => () =>
      saveResearch(
        {
          ...research,
          root: user.root === "0" ? user.id : user.root
        },
        research._id
          ? () => history.push(`${basePath}`)
          : research => history.push(`${basePath}/edit/${research._id}`)
      ),
    onDelete: () => () => {}
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params;
      if (!isEmpty(id)) {
        this.props.getResearch(id);
      }
    },
    componentDidUpdate() {
      const { isLoadedResearch, research } = this.props;
      if (!isLoadedResearch && research._id) {
        this.props.setIsLoadedResearch(true);
        this.props.getSurveys(research._id);
      }
    },
    componentWillUnmount() {
      this.props.storeResearch({});
    }
  }),
  withStyles(theme => ({ ...CommonStyles(theme), ...styles(theme) }), {
    theme: true
  })
)(ResearchEdit);
