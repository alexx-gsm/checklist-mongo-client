import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import classNames from 'classnames'
// Components
import TopPanel from '../../../../components/TopPanel'
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
// Material UI
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import InputAdornment from '@material-ui/core/InputAdornment'
import Fab from '@material-ui/core/Fab'
import Button from '@material-ui/core/Button'
import MenuItem from '@material-ui/core/MenuItem'
import Divider from '@material-ui/core/Divider'
import { List, ListItem } from '@material-ui/core'
import { MuiThemeProvider } from '@material-ui/core/styles'
// Material UI/icons
import AddIcon from '@material-ui/icons/Add'
// React icons kit
import { Icon } from 'react-icons-kit'
import { envelopeO } from 'react-icons-kit/fa/envelopeO'
import { trashO } from 'react-icons-kit/fa/trashO'
import { key } from 'react-icons-kit/fa/key'
// useful tools
import isEmpty from '../../../../helpers/is-empty'

const ResearchEdit = ({
  research,
  surveys,
  error,
  classes,
  onChange,
  handleClick,
  handleAddClick,
  onSave,
  onDelete,
  loading,
  basePath,
  theme
}) => {
  return (
    <MuiThemeProvider theme={theme}>
      <Grid className={classes._Wrap}>
        {!loading ? (
          <React.Fragment>
            {/* <TopPanel title='Исследование' /> */}

            <form
              noValidate
              className={classes._Form}
              noValidate
              autoComplete='off'
            >
              <TextField
                fullWidth
                id='title'
                label='Название исследования'
                value={isEmpty(research.title) ? '' : research.title}
                onChange={onChange('title')}
                margin='dense'
                InputLabelProps={{
                  FormLabelClasses: {
                    root: classes.labelPrimary,
                    focused: classes.cssFocused
                  }
                }}
                InputProps={{
                  className: classes._FormTitle,
                  classes: { underline: classes.underlinePrimary }
                }}
                error={Boolean(error.title)}
                helperText={error.title ? error.title : null}
              />
              {/* <TextField
                fullWidth
                multiline
                rowsMax='4'
                id='comment'
                label='Комментарий'
                value={isEmpty(research.comment) ? '' : research.comment}
                onChange={onChange('comment')}
                margin='dense'
                InputLabelProps={{
                  FormLabelClasses: {
                    root: classes.labelPrimary,
                    focused: classes.cssFocused
                  }
                }}
                InputProps={{
                  classes: { underline: classes.underlinePrimary }
                }}
                error={Boolean(error.comment)}
                helperText={error.comment ? error.comment : null}
              /> */}

              <Paper className={classes.MT20}>
                <Typography variant='h4' align='center'>
                  Опросы
                </Typography>
                {surveys && (
                  <List>
                    {surveys.map(survey => (
                      <ListItem
                        key={survey._id}
                        button
                        divider
                        onClick={handleClick(survey._id)}
                      >
                        <Typography variant='h6' align='right'>
                          {survey.title}
                        </Typography>
                      </ListItem>
                    ))}
                  </List>
                )}
                <Grid container className={classes.P20}>
                  <Fab
                    color='primary'
                    aria-label='Add'
                    size='large'
                    className={classes.fab}
                    onClick={handleAddClick}
                    disabled={!research._id}
                  >
                    <AddIcon />
                  </Fab>
                </Grid>
              </Paper>
            </form>

            <Grid container className={classes._ActionGrid} spacing={8}>
              <Grid item>
                <Button
                  aria-label='toList'
                  variant='outlined'
                  to={`${basePath}`}
                  component={Link}
                  fullWidth
                >
                  К списку
                </Button>
              </Grid>
              <Grid item className={classes._ActionGridLeftAuto}>
                <Button
                  to={basePath}
                  component={Link}
                  aria-label='Back'
                  variant='contained'
                  fullWidth
                >
                  Отмена
                </Button>
              </Grid>
              <Grid item>
                <Button
                  onClick={onSave}
                  aria-label='Save'
                  variant='contained'
                  color='primary'
                  fullWidth
                >
                  Сохранить исследование
                </Button>
              </Grid>
            </Grid>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <Typography variant='h6' className={classes._PageTitle}>
              загрузка...
            </Typography>
            <LinearIndeterminate />
          </React.Fragment>
        )}
      </Grid>
    </MuiThemeProvider>

    //   <Divider />

    //   <Grid container className={classes.cardFooter}>
    //     <Grid item>
    //       <Fab
    //         size='small'
    //         aria-label='Delete'
    //         className={classes.buttonDelete}
    //         onClick={onDelete}
    //       >
    //         <Icon size={18} icon={trashO} />
    //       </Fab>
    //     </Grid>
    //     <Grid item>

    //     </Grid>
    //   </Grid>
    // </Paper>
  )
}

ResearchEdit.defaultProps = {
  reseach: {},
  surveys: [],
  error: {}
}

ResearchEdit.propTypes = {
  user: PropTypes.object,
  roles: PropTypes.object,
  loading: PropTypes.bool,
  error: PropTypes.object,
  classes: PropTypes.object.isRequired,
  onReload: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  basePath: PropTypes.string
}

export default ResearchEdit
