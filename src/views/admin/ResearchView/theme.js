import { createMuiTheme } from '@material-ui/core/styles'
import { blueGrey } from '@material-ui/core/colors'

export default createMuiTheme({
  typography: {
    useNextVariants: true
  },
  palette: {
    primary: {
      main: '#546e7a',
      contrastText: '#fff'
    },
    secondary: {
      main: '#d50000',
      contrastText: '#fff'
    },
    extraColor: blueGrey
  }
})
