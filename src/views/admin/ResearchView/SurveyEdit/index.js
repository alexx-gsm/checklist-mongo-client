import { compose, withHandlers, lifecycle } from 'recompose'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
// AC
import { getResearch } from '../../../../redux/modules/research'
import {
  getOneSurvey,
  storeSurvey,
  saveSurvey
} from '../../../../redux/modules/survey'
import { getQuestions } from '../../../../redux/modules/question'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CommonStyles from '../../../../styles/CommonStyles'

import SurveyEdit from './SurveyEdit'

export default compose(
  connect(
    ({ researchStore, surveyStore }) => {
      const { research } = researchStore
      const { survey, loading, error } = surveyStore
      return {
        research,
        survey,
        loading: loading && researchStore.loading,
        error
      }
    },
    { getResearch, getOneSurvey, storeSurvey, saveSurvey, getQuestions }
  ),
  withRouter,
  withHandlers({
    onReload: ({}) => () => {},
    onChange: ({ survey, storeSurvey }) => index => event => {
      storeSurvey({
        ...survey,
        [index]: event.target.value
      })
    },
    handleClick: ({ basePath, history }) => id => () =>
      history.push(`${basePath}/edit/${id}`),
    handleAddClick: ({ history, basePath, research }) => () => {
      history.push(`${basePath}/${research._id}/edit`)
    },
    handleRun: ({ survey, storeSurvey }) => e =>
      storeSurvey({
        ...survey,
        isRun: e.target.checked
      }),
    onSave: ({ research, survey, saveSurvey, history, basePath }) => () =>
      saveSurvey(
        {
          ...survey,
          rid: survey.rid ? survey.rid : research._id
        },
        !survey._id
          ? item => history.push(`${basePath}/${research._id}/${item._id}`)
          : () => history.push(`${basePath}/edit/${research._id}`)
      ),
    onDelete: () => () => {}
  }),
  lifecycle({
    componentDidMount() {
      const { rid, id } = this.props.match.params
      this.props.getResearch(rid)
      if (id) {
        this.props.getOneSurvey(id)
      }
    },
    componentWillUnmount() {
      this.props.storeSurvey({})
    }
  }),
  withStyles(theme => ({ ...CommonStyles(theme), ...styles(theme) }), {
    theme: true
  })
)(SurveyEdit)
