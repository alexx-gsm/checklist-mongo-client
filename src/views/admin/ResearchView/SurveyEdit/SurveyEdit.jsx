import React, { Fragment } from 'react'
import { Route, Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import classNames from 'classnames'
// Components
import SurveyTabs from '../SurveyTabs'
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
// Material UI
import { Grid, Typography, TextField, Button } from '@material-ui/core'
import { Switch, FormGroup, FormControlLabel } from '@material-ui/core'
import { MuiThemeProvider } from '@material-ui/core/styles'
// React icons kit
import { Icon } from 'react-icons-kit'
import { envelopeO, key } from 'react-icons-kit/fa'
// useful tools
import isEmpty from '../../../../helpers/is-empty'

import tabs from '../SurveyTabs/tabs'

const SurveyEdit = ({
  research,
  survey,
  error,
  classes,
  onChange,
  handleRun,
  onSave,
  loading,
  basePath,
  theme,
  match
}) => {
  const { rid } = match.params
  return (
    <MuiThemeProvider theme={theme}>
      <Grid className={classes._Wrap}>
        {!loading ? (
          <Fragment>
            <Typography variant='h5'>{research.title}</Typography>

            <form
              noValidate
              className={classes._Form}
              noValidate
              autoComplete='off'
            >
              <Grid container spacing={8} alignItems='center'>
                <Grid item xs>
                  <TextField
                    fullWidth
                    id='title'
                    label='Название опроса'
                    value={isEmpty(survey.title) ? '' : survey.title}
                    onChange={onChange('title')}
                    margin='normal'
                    className={classes.textName}
                    InputLabelProps={{
                      FormLabelClasses: {
                        root: classes.labelPrimary,
                        focused: classes.cssFocused
                      }
                    }}
                    InputProps={{
                      className: classes.inputName,
                      classes: { underline: classes.underlinePrimary }
                    }}
                    error={Boolean(error.title)}
                    helperText={error.title ? error.title : null}
                  />
                </Grid>
                <Grid item>
                  <FormGroup row>
                    <FormControlLabel
                      labelPlacement='start'
                      control={
                        <Switch
                          checked={
                            !isEmpty(survey.isRun) ? survey.isRun : false
                          }
                          onChange={handleRun}
                        />
                      }
                      label='Start'
                    />
                  </FormGroup>
                </Grid>
              </Grid>

              {survey._id && (
                <SurveyTabs url={`${basePath}/${rid}/${survey._id}`} />
              )}
            </form>
            <Route
              exact
              path={tabs.map(tab => `${match.url}${tab.link}`)}
              render={() => (
                <Grid container className={classes._ActionGrid} spacing={8}>
                  <Grid item>
                    <Button
                      variant='outlined'
                      to={`${basePath}/edit/${rid}`}
                      component={Link}
                      fullWidth
                    >
                      К исследованию
                    </Button>
                  </Grid>
                  <Grid item className={classes._ActionGridLeftAuto}>
                    <Button
                      to={`${basePath}/edit/${rid}`}
                      component={Link}
                      aria-label='Back'
                      variant='contained'
                      fullWidth
                    >
                      Отмена
                    </Button>
                  </Grid>
                  <Grid item>
                    <Button
                      onClick={onSave}
                      aria-label='Save'
                      variant='contained'
                      color='primary'
                      fullWidth
                    >
                      Сохранить опрос
                    </Button>
                  </Grid>
                </Grid>
              )}
            />
          </Fragment>
        ) : (
          <Fragment>
            <Typography variant='h6' className={classes._PageTitle}>
              загрузка...
            </Typography>
            <LinearIndeterminate />
          </Fragment>
        )}
      </Grid>
    </MuiThemeProvider>
  )
}

SurveyEdit.defealtProps = {
  research: {},
  survey: {}
}

export default SurveyEdit
