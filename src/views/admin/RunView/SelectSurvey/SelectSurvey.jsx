import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import classNames from 'classnames'
// Components
import TopPanel from '../../../../components/TopPanel'
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
// @material-ui components
import { TextField, Grid, Typography, Fab, Button } from '@material-ui/core'
import { List, ListItem, MenuItem } from '@material-ui/core'
// @material-ui icons
import { PlayArrow, ShowChart } from '@material-ui/icons'
import { MuiThemeProvider } from '@material-ui/core/styles'
// useful tool
import isEmpty from '../../../../helpers/is-empty'

const SelectSurvey = ({
  researches,
  surveys,
  respondents,
  share,
  storeShare,
  url,
  loading,
  error,
  classes,
  theme
}) => {
  const { researchId, surveyId, respondentId } = share
  return (
    <MuiThemeProvider theme={theme}>
      <Grid className={classes._Wrap}>
        {!loading ? (
          <React.Fragment>
            <TextField
              select
              fullWidth
              id='researchId'
              label='Исследование'
              value={isEmpty(researchId) ? '' : researchId}
              onChange={e => storeShare({ researchId: e.target.value })}
              margin='normal'
              className={classes.wrapTitle}
            >
              {researches.map(item => {
                return (
                  <MenuItem key={item._id} value={item._id}>
                    <Grid
                      container
                      alignItems='baseline'
                      className={classes.gridType}
                      direction='row'
                      wrap='wrap'
                    >
                      <Typography
                        className={classes.typoSelectInputTitle}
                        variant='h6'
                      >
                        {item.title}
                      </Typography>
                    </Grid>
                  </MenuItem>
                )
              })}
            </TextField>

            {!isEmpty(researchId) && (
              <TextField
                select
                fullWidth
                id='surveyId'
                label='Опрос'
                value={isEmpty(surveyId) ? '' : surveyId}
                onChange={e => storeShare({ surveyId: e.target.value })}
                margin='normal'
                className={classes.wrapTitle}
                InputProps={{
                  className: classes.Title
                }}
                error={Boolean(error.answerId)}
                helperText={error.answerId ? error.answerId : null}
              >
                {surveys.map(item => {
                  return (
                    <MenuItem key={item._id} value={item._id}>
                      <Grid
                        container
                        alignItems='baseline'
                        className={classes.gridType}
                        direction='row'
                        wrap='wrap'
                      >
                        <Typography
                          className={classes.typoSelectInputTitle}
                          variant='h6'
                        >
                          {item.title}
                        </Typography>
                      </Grid>
                    </MenuItem>
                  )
                })}
              </TextField>
            )}

            {!isEmpty(surveyId) && (
              <TextField
                select
                fullWidth
                id='respondentId'
                label='Респондент'
                value={isEmpty(respondentId) ? '' : respondentId}
                onChange={e => storeShare({ respondentId: e.target.value })}
                margin='normal'
                className={classes.wrapTitle}
                InputProps={{
                  className: classes.Title
                }}
                error={Boolean(error.answerId)}
                helperText={error.answerId ? error.answerId : null}
              >
                {respondents.map(item => {
                  return (
                    <MenuItem key={item._id} value={item._id}>
                      <Grid
                        container
                        alignItems='baseline'
                        className={classes.gridType}
                        direction='row'
                        wrap='wrap'
                      >
                        <Typography
                          className={classes.typoSelectInputTitle}
                          variant='h6'
                        >
                          {item.code} {item.name}
                        </Typography>
                      </Grid>
                    </MenuItem>
                  )
                })}
              </TextField>
            )}

            {!isEmpty(respondentId) && (
              <Grid className={classes.MT20}>
                <Button
                  variant='outlined'
                  color='secondary'
                  to={`/run/${surveyId}/${respondentId}`}
                  component={Link}
                >
                  Запустить
                </Button>
              </Grid>
            )}
          </React.Fragment>
        ) : (
          <React.Fragment>
            <Typography variant='h6' className={classes._PageTitle}>
              загрузка...
            </Typography>
            <LinearIndeterminate />
          </React.Fragment>
        )}
      </Grid>
    </MuiThemeProvider>

    // {surveys.map(survey => {
    //   return (
    //     <Grid container alignItems='center' spacing={8} key={survey._id}>
    //       <Grid item>
    //         <Fab
    //           size='small'
    //           aria-label='Back'
    //           className={classes.buttonReturn}
    //           to={`${basePath}/${survey._id}`}
    //           component={Link}
    //           color='primary'
    //         >
    //           <PlayArrow />
    //         </Fab>
    //       </Grid>
    //       <Grid item>
    //         <Typography variant='h5'>{survey.title}</Typography>
    //       </Grid>
    //     </Grid>
    //   )
    // })}
  )
}

SelectSurvey.defaultProps = {
  researches: [],
  surveys: [],
  respondents: []
}

export default SelectSurvey
