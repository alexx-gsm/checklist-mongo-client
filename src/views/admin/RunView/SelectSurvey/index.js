import { compose, withHandlers, lifecycle, withState } from 'recompose'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
// AC
import { storeShare } from '../../../../redux/modules/share'
import { getResearches } from '../../../../redux/modules/research'
import { getSurveys } from '../../../../redux/modules/survey'
import { getRespondents } from '../../../../redux/modules/respondent'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CommonStyles from '../../../../styles/CommonStyles'

import SelectSurvey from './SelectSurvey'

export default compose(
  connect(
    ({ researchStore, surveyStore, respondentStore, shareStore }) => {
      const { researches, loading, error } = researchStore
      const { surveys } = surveyStore
      const { respondents } = respondentStore

      const { share } = shareStore

      return {
        researches,
        surveys: share.researchId
          ? surveys.filter(s => s.rid === share.researchId)
          : [],
        respondents: share.surveyId
          ? respondents.filter(s => s.sid === share.surveyId)
          : [],
        share,
        error,
        loading: loading
      }
    },
    { getResearches, getSurveys, getRespondents, storeShare }
  ),
  withRouter,
  lifecycle({
    componentDidMount() {
      this.props.getResearches()
      this.props.getSurveys()
      this.props.getRespondents()
    }
  }),
  withStyles(theme => ({ ...CommonStyles(theme), ...styles(theme) }), {
    theme: true
  })
)(SelectSurvey)
