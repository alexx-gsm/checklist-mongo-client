import React, { lazy, Suspense } from 'react'
import { Route, Switch } from 'react-router-dom'
// components
import LinearIndeterminate from '../../../components/LinearIndeterminate'
// theme
import theme from './theme'

const SelectSurvey = lazy(() => import('./SelectSurvey'))
// const ResearchEdit = lazy(() => import('./ResearchEdit'))
// const SurveyEdit = lazy(() => import('./SurveyEdit'))

const basePath = '/admin/run'

const ResearchView = () => (
  <Suspense fallback={<LinearIndeterminate />}>
    <Switch>
      <Route
        exact
        path={`${basePath}`}
        render={() => <SelectSurvey url={basePath} theme={theme} />}
      />
      {/* <Route
        path={`${basePath}/edit/:id?`}
        render={() => <ResearchEdit basePath={basePath} theme={theme} />}
      />
      <Route
        path={`${basePath}/:rid/:id?`}
        render={() => <SurveyEdit basePath={basePath} theme={theme} />}
      /> */}
    </Switch>
  </Suspense>
)

export default ResearchView
