import { withRouter } from 'react-router-dom'
import {
  compose,
  withProps,
  withHandlers,
  lifecycle,
  withState
} from 'recompose'
import { connect } from 'react-redux'
// AC
import { getUsers, storeUser } from '../../../../redux/modules/auth'
// Material UI
import { withStyles } from '@material-ui/core'
// Styles
import CommonStyles from '../../../../styles/CommonStyles'
import styles from './styles'

import UserList from './UserList'

export default compose(
  connect(
    ({ auth }) => {
      const { users, loading, error } = auth

      return {
        users,
        loading,
        error
      }
    },
    { getUsers, storeUser }
  ),
  withRouter,
  withProps({
    formatter: new Intl.NumberFormat('ru-ru', {
      style: 'currency',
      currency: 'RUB',
      minimumFractionDigits: 0
    })
  }),
  withState('selectedCategory', 'setSelectedCategory', '-'),
  withHandlers({
    handleClick: ({ basePath, history }) => id => () =>
      history.push(`${basePath}/edit/${id}`),
    handleFilter: ({ setSelectedCategory }) => e =>
      setSelectedCategory(e.target.value),
    onReload: ({}) => () => {}
  }),
  lifecycle({
    componentDidMount() {
      this.props.getUsers()
      this.props.storeUser({})
    }
  }),
  withStyles(
    theme => ({
      ...CommonStyles(theme),
      ...styles(theme)
    }),
    { withTheme: true }
  )
)(UserList)
