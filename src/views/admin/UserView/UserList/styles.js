export default theme => ({
  Wrap: {
    width: '100%',
    maxWidth: '960px'
  },
  Select: {
    marginTop: theme.spacing.unit * 3,
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    minWidth: '200px'
  },
  SelectInput: {
    paddingTop: theme.spacing.unit,
    paddingBottom: theme.spacing.unit
  },
  PaperDishes: {
    padding: theme.spacing.unit,
    marginBottom: theme.spacing.unit * 2,
    background: theme.palette.grey[50]
  }
})
