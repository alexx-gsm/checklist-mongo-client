import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
// @material-ui components
import { Grid, Typography, Button, Paper } from '@material-ui/core'
import { List, ListItem, ListItemText } from '@material-ui/core'
import { TextField, MenuItem } from '@material-ui/core'
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
import { MuiThemeProvider } from '@material-ui/core/styles'
import TopPanel from '../../../../components/TopPanel'
// useful tools
import isEmpty from '../../../../helpers/is-empty'

const UserList = ({
  users,
  basePath,
  handleClick,
  loading,
  error,
  classes,
  theme
}) => {
  return (
    <MuiThemeProvider theme={theme}>
      <Grid className={classes.Wrap}>
        {!loading ? (
          <React.Fragment>
            <List>
              {users.map(user => {
                return (
                  <ListItem
                    key={user._id}
                    button
                    divider
                    onClick={handleClick(user._id)}
                  >
                    <Grid
                      container
                      wrap='nowrap'
                      spacing={16}
                      justify='space-between'
                      alignItems='center'
                    >
                      <Grid item container alignItems='center'>
                        <Typography variant='h6' align='right'>
                          {user.name}
                        </Typography>
                        {user.root === '0' && (
                          <Typography
                            variant='caption'
                            style={{ padding: '0 3px' }}
                          >
                            (root)
                          </Typography>
                        )}
                      </Grid>
                      <Grid item>
                        <Typography variant='caption' align='right'>
                          {user.role}
                        </Typography>
                      </Grid>
                    </Grid>
                  </ListItem>
                )
              })}
            </List>
            <Grid className={classes.MT20}>
              <Button
                variant='outlined'
                color='secondary'
                to={`${basePath}/edit`}
                component={Link}
              >
                Добавить
              </Button>
            </Grid>
          </React.Fragment>
        ) : (
          <React.Fragment>
            <Typography variant='h6' className={classes._PageTitle}>
              загрузка...
            </Typography>
            <LinearIndeterminate />
          </React.Fragment>
        )}
      </Grid>
    </MuiThemeProvider>
  )
}

UserList.defaultProps = {
  users: []
}

UserList.propTypes = {
  dishes: PropTypes.arrayOf(PropTypes.object),
  loading: PropTypes.bool,
  classes: PropTypes.object
}

export default UserList
