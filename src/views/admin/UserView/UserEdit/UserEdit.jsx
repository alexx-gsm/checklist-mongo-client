import React, { Fragment } from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import classNames from 'classnames'
// Components
import TopPanel from '../../../../components/TopPanel'
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
// Material UI
import { Grid, Typography, TextField, InputAdornment } from '@material-ui/core'
import { Switch, Button, MenuItem } from '@material-ui/core'
import { FormGroup, FormControlLabel } from '@material-ui/core'
import { MuiThemeProvider } from '@material-ui/core/styles'
// React icons kit
import { Icon } from 'react-icons-kit'
import { envelopeO, key } from 'react-icons-kit/fa'
// useful tools
import isEmpty from '../../../../helpers/is-empty'

const UserEdit = ({
  user,
  editedUser,
  roles,
  isRoot,
  setIsRoot,
  error,
  classes,
  onChange,
  onSwitch,
  onSave,
  loading,
  basePath,
  theme
}) => {
  return (
    <MuiThemeProvider theme={theme}>
      <Grid className={classes.Wrap}>
        {!loading ? (
          <Fragment>
            <TopPanel title='Пользователь' />

            <form
              noValidate
              className={classes.Form}
              noValidate
              autoComplete='off'
            >
              <TextField
                fullWidth
                id='name'
                label='ФИО'
                value={isEmpty(editedUser.name) ? '' : editedUser.name}
                onChange={onChange('name')}
                margin='normal'
                className={classes.textName}
                InputLabelProps={{
                  FormLabelClasses: {
                    root: classes.labelPrimary,
                    focused: classes.cssFocused
                  }
                }}
                InputProps={{
                  className: classes.inputName,
                  classes: { underline: classes.underlinePrimary }
                }}
                error={Boolean(error.name)}
                helperText={error.name ? error.name : null}
              />

              <Grid container spacing={16} alignItems='flex-end'>
                <Grid item xs={12} sm={10}>
                  <TextField
                    select
                    fullWidth
                    id='role-select'
                    label='Роль'
                    value={isEmpty(editedUser.role) ? '' : editedUser.role}
                    onChange={onChange('role')}
                    margin='normal'
                    className={classes.wrapTitle}
                    InputLabelProps={{
                      // shrink: true,
                      FormLabelClasses: {
                        root: classes.labelPrimary,
                        focused: classes.cssFocused
                      }
                    }}
                    InputProps={{
                      className: classes.inputTitle,
                      classes: { underline: classes.underlinePrimary }
                    }}
                    SelectProps={{
                      classes: {
                        select: classes.selectInput
                      }
                    }}
                    error={Boolean(error.role)}
                    helperText={error.role ? error.role : null}
                  >
                    {roles.map(role => {
                      return (
                        <MenuItem key={role.alias} value={role.alias}>
                          <Grid
                            container
                            alignItems='baseline'
                            className={classes.gridType}
                            direction='row'
                            wrap='wrap'
                          >
                            <Typography
                              className={classes.typoSelectInputTitle}
                              variant='title'
                            >
                              {role.title}
                            </Typography>
                            <Typography
                              className={classes.typoSelectInputSubtitle}
                              variant='caption'
                              style={{ marginLeft: '5px' }}
                            >
                              ({role.alias})
                            </Typography>
                          </Grid>
                        </MenuItem>
                      )
                    })}
                  </TextField>
                </Grid>
                <Grid item xs={12} sm={2}>
                  {user.root === '0' && (
                    <FormGroup row>
                      <FormControlLabel
                        control={
                          <Switch
                            checked={editedUser.root === '0'}
                            onChange={onSwitch}
                          />
                        }
                        label='Root'
                        labelPlacement='start'
                      />
                    </FormGroup>
                  )}
                </Grid>
              </Grid>

              <TextField
                fullWidth
                id='email'
                label='Почта'
                type='email'
                value={isEmpty(editedUser.email) ? '' : editedUser.email}
                onChange={onChange('email')}
                className={classes.select}
                margin='normal'
                InputLabelProps={{
                  FormLabelClasses: {
                    root: classes.labelPrimary,
                    focused: classes.cssFocused
                  }
                }}
                InputProps={{
                  classes: { underline: classes.underlinePrimary },
                  endAdornment: (
                    <InputAdornment position='end'>
                      <Icon icon={envelopeO} />
                    </InputAdornment>
                  )
                }}
                error={Boolean(error.email)}
                helperText={error.email ? error.email : null}
              />

              {!editedUser._id && (
                <Fragment>
                  <TextField
                    fullWidth
                    id='password'
                    label='Пароль'
                    type='password'
                    value={
                      isEmpty(editedUser.password) ? '' : editedUser.password
                    }
                    onChange={onChange('password')}
                    className={classes.select}
                    margin='normal'
                    InputLabelProps={{
                      FormLabelClasses: {
                        root: classes.labelPrimary,
                        focused: classes.cssFocused
                      }
                    }}
                    InputProps={{
                      classes: { underline: classes.underlinePrimary },
                      endAdornment: (
                        <InputAdornment position='end'>
                          <Icon icon={key} />
                        </InputAdornment>
                      )
                    }}
                    error={Boolean(error.password)}
                    helperText={error.password ? error.password : null}
                  />
                  <TextField
                    fullWidth
                    id='password2'
                    label='Пароль 2'
                    type='password'
                    value={
                      isEmpty(editedUser.password2) ? '' : editedUser.password2
                    }
                    onChange={onChange('password2')}
                    className={classes.select}
                    margin='normal'
                    InputLabelProps={{
                      FormLabelClasses: {
                        root: classes.labelPrimary,
                        focused: classes.cssFocused
                      }
                    }}
                    InputProps={{
                      classes: { underline: classes.underlinePrimary },
                      endAdornment: (
                        <InputAdornment position='end'>
                          <Icon icon={key} />
                        </InputAdornment>
                      )
                    }}
                    error={Boolean(error.password2)}
                    helperText={error.password2 ? error.password2 : null}
                  />
                </Fragment>
              )}

              <Grid container className={classes.MT40} justify='space-between'>
                <Grid item>
                  <Button
                    variant='outlined'
                    to={`${basePath}`}
                    component={Link}
                  >
                    К списку
                  </Button>
                </Grid>
                <Grid item>
                  <Button
                    to={basePath}
                    component={Link}
                    aria-label='Back'
                    variant='contained'
                  >
                    Отмена
                  </Button>
                  <Button
                    onClick={onSave}
                    aria-label='Save'
                    variant='contained'
                    className={classNames(classes.btnPrimary)}
                    color='primary'
                    // disabled={!(editedUser.image || croppedImg)}
                  >
                    Сохранить
                  </Button>
                </Grid>
              </Grid>
            </form>
          </Fragment>
        ) : (
          <Fragment>
            <Typography variant='h6' className={classes._PageTitle}>
              загрузка...
            </Typography>
            <LinearIndeterminate />
          </Fragment>
        )}
      </Grid>
    </MuiThemeProvider>
  )
}

UserEdit.defaultProps = {
  user: {},
  editedUser: {},
  roles: [],
  error: {}
}

UserEdit.propTypes = {
  editedUser: PropTypes.object,
  roles: PropTypes.arrayOf(PropTypes.object),
  loading: PropTypes.bool,
  error: PropTypes.object,
  classes: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
  onSave: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  basePath: PropTypes.string
}

export default UserEdit
