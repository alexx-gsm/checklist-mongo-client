export default theme => ({
  Wrap: {
    maxWidth: '960px'
  },
  Form: {
    marginBottom: theme.spacing.unit * 4,
    [theme.breakpoints.down('xs')]: {
      paddingLeft: '8px',
      paddingRight: '8px'
    }
  },
  textName: {},
  inputName: {
    fontSize: '46px',
    color: '#666',
    '& input': { padding: 0 },
    [theme.breakpoints.down('xs')]: {
      fontSize: 28
    }
  },
  // CARD FOOTER
  cardFooter: {
    padding: '10px 20px',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  // buttonDelete: {
  //   color: 'white',
  //   backgroundColor: red[600],
  //   '&:hover': { backgroundColor: red[700] }
  // },
  btnPrimary: {
    marginLeft: '10px'
  },
  //
  PaperLogin: {
    marginTop: '20px',
    padding: '10px 20px 20px',
    background: theme.palette.grey[50]
  },
  // img edit
  CropperWrap: {
    width: '100%',
    height: '400px',
    position: 'relative',
    marginTop: '10px'
  },

  CropperControls: {
    margin: 'auto',
    width: '50%',
    height: '80px',
    display: 'flex',
    alignItems: 'center'
  },
  Image: {
    maxWidth: '400px',
    '& img': {
      maxWidth: '100%'
    }
  },
  CropperSlider: {
    padding: '22px 0px'
  },
  Upload: {
    display: 'none'
  },
  UploadButton: {
    margin: `${theme.spacing.unit}px 0`
  },
  UploadList: {
    maxHeight: '60vh',
    overflow: 'auto'
  }
})
