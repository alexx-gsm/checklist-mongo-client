import {
  compose,
  withState,
  withHandlers,
  lifecycle,
  withProps
} from 'recompose'
import { withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
// AC
import {
  getUserById,
  storeUser,
  registerUser,
  updateUser
} from '../../../../redux/modules/auth'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CommonStyles from '../../../../styles/CommonStyles'
// Useful tools
import isEmpty from '../../../../helpers/is-empty'

import UserEdit from './UserEdit'

export default compose(
  connect(
    ({ auth, roleStore }) => {
      const { user, editedUser, loading, error } = auth
      const { roles } = roleStore
      return {
        user,
        editedUser,
        roles,
        error,
        loading
      }
    },
    { getUserById, storeUser, registerUser, updateUser }
  ),
  withRouter,
  withState('isRoot', 'setIsRoot', ({ editedUser }) => {
    console.log('--- editedUser', editedUser)
    return editedUser.root === '0'
  }),
  withHandlers({
    onReload: ({}) => () => {},
    onChange: ({ editedUser, storeUser }) => index => event => {
      storeUser({
        ...editedUser,
        [index]: event.target.value
      })
    },
    onSwitch: ({ user, editedUser, storeUser }) => event => {
      storeUser({
        ...editedUser,
        root: event.target.checked ? '0' : user.id
      })
    },
    onSave: ({
      user,
      editedUser,
      isRoot,
      registerUser,
      updateUser,
      history,
      basePath
    }) => () => {
      const newUser = {
        ...editedUser,
        root: isEmpty(editedUser.root) ? user.id : editedUser.root
      }

      if (editedUser._id) {
        updateUser(newUser, () => history.push(basePath))
      } else {
        registerUser(newUser, () => history.push(basePath))
      }
    },
    onDelete: () => () => {}
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params
      if (id) {
        this.props.getUserById(id)
      }
    }
  }),
  withStyles(theme => ({ ...CommonStyles(theme), ...styles(theme) }), {
    theme: true
  })
)(UserEdit)
