import React, { lazy, Suspense } from 'react'
import { Route } from 'react-router-dom'
// components
import LinearIndeterminate from '../../../components/LinearIndeterminate'
// theme
import theme from './theme'

const Run = lazy(() => import('./Run'))

const basePath = '/run'

const RunView = () => (
  <Suspense fallback={<LinearIndeterminate />}>
    <Route
      path={`${basePath}/:id?`}
      render={() => <Run basePath={basePath} theme={theme} />}
    />
  </Suspense>
)

export default RunView
