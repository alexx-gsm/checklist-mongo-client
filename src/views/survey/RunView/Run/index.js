import React from 'react'
import { withRouter } from 'react-router-dom'
import {
  compose,
  withState,
  withHandlers,
  withProps,
  lifecycle
} from 'recompose'
import { connect } from 'react-redux'
// AC
import {
  getData,
  getOneResult,
  storeResult
} from '../../../../redux/modules/result'
import {
  getOneSurvey,
  setSurveyStage,
  storeAndUpdateSurvey,
  storePrevResult,
  storeStageResult
} from '../../../../redux/modules/survey'
import { getQuestions } from '../../../../redux/modules/question'
import {
  getOneRespondent,
  getRespondents
} from '../../../../redux/modules/respondent'
import { saveResult } from '../../../../redux/modules/result'
// Styles
import { withStyles } from '@material-ui/core/styles'
import CommonStyles from '../../../../styles/CommonStyles'
import СardStyles from '../../../../styles/CardStyles'
import CardTableStyles from '../../../../styles/CardTableStyles'
import styles from './styles'
// useful tools
import isEmpty from '../../../../helpers/is-empty'

import Run from './Run'

export default compose(
  connect(
    ({ resultStore, surveyStore, questionStore, respondentStore }) => {
      const { survey, stage, result, error, loading } = surveyStore
      const { questions, answers } = questionStore
      const { respondent } = respondentStore

      const question =
        !isEmpty(questions) && stage < questions.length ? questions[stage] : {}

      return {
        _result: resultStore.result,
        stage,
        survey,
        result,
        questions,
        question,
        answers,
        respondent,
        error,
        loading
      }
    },
    {
      getData,
      getOneResult,
      storeResult,
      getOneSurvey,
      setSurveyStage,
      storeAndUpdateSurvey,
      storePrevResult,
      storeStageResult,
      getQuestions,
      getOneRespondent,
      getRespondents,
      saveResult
    }
  ),
  withRouter,
  withState('showSelect', 'setShowSelect', false),
  withHandlers({
    handleNext: ({
      stage,
      result,
      questions,
      question,
      setSurveyStage,
      storeStageResult,
      storePrevResult,
      _result,
      saveResult
    }) => () => {
      if (question && question.type === '1') {
        storePrevResult(result)
      } else {
        storePrevResult([])
      }

      if (stage < questions.length - 1) {
        const { summary = {} } = _result

        saveResult({
          ..._result,
          summary: {
            ...summary,
            [question._id]: result
          }
        })

        const qid =
          questions && questions[stage + 1] ? questions[stage + 1]._id : null

        const nextResult =
          qid && _result && _result.summary && _result.summary[qid]
            ? _result.summary[qid]
            : []

        storeStageResult(nextResult)
        setSurveyStage(stage + 1)
      }
    },
    handleBack: ({
      stage,
      setSurveyStage,
      survey,
      result,
      respondent,
      answers,
      questionList,
      storeStageResult,
      saveResult,
      storePrevResult,
      storeAndUpdateSurvey,
      question,
      questions,
      _result
    }) => () => {
      if (stage > 0) {
        const { summary = {} } = _result
        saveResult({
          ..._result,
          summary: {
            ...summary,
            [question._id]: result
          }
        })
        const qid =
          questions && questions[stage - 1] ? questions[stage - 1]._id : null

        const prevResult =
          qid && _result && _result.summary ? _result.summary[qid] : []

        storeStageResult(prevResult)
        setSurveyStage(stage - 1)

        // if (stage - 2 >= 0) {
        //   const prevQuestionId = survey ? survey.questions[stage - 1] : null
        //   const prevAnswer = prevQuestionId
        //     ? answers[questionList[prevQuestionId].answerId]
        //     : null

        //   const prevPrevQuestionId = survey ? survey.questions[stage - 2] : null
        //   const prevPrevAnswer = prevPrevQuestionId
        //     ? answers[questionList[prevPrevQuestionId].answerId]
        //     : null

        //   if (
        //     prevAnswer &&
        //     prevAnswer.isMembersInQuestion &&
        //     prevPrevAnswer &&
        //     prevPrevAnswer.isMembersInQuestion
        //   ) {
        //     const prevPrevResult = survey.results[respondent.code][stage - 2]
        //     storePrevResult(prevPrevResult)
        //   } else {
        //     storePrevResult([])
        //   }
        // }
      }
    }
  }),
  lifecycle({
    componentDidMount() {
      const { id } = this.props.match.params

      console.log('id', id)
      if (!isEmpty(id)) {
        this.props.getData(id)
      } else {
        window.location.replace(window.location.origin)
      }
    },
    componentDidUpdate(prev) {
      if (isEmpty(prev._result) && !isEmpty(this.props._result)) {
        const { surveyId, respondentId } = this.props._result
        this.props.getOneSurvey(surveyId)
        this.props.getQuestions(surveyId)
        this.props.getRespondents(surveyId)
        this.props.getOneRespondent(respondentId)
      }

      if (isEmpty(prev.questions) && !isEmpty(this.props.questions)) {
        const { summary } = this.props._result
        const { questions } = this.props
        const initialStage = 0

        const qid =
          questions && questions[initialStage]
            ? questions[initialStage]._id
            : null

        const result = qid && summary ? summary[qid] : []

        this.props.storeStageResult(result)
        this.props.setSurveyStage(initialStage)
      }
    }
  }),
  withStyles(
    theme => {
      return {
        ...СardStyles(theme),
        ...CommonStyles(theme),
        ...styles(theme)
      }
    },
    { theme: true }
  )
)(Run)
