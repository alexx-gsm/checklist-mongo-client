import React from 'react'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import LinearIndeterminate from '../../../../components/LinearIndeterminate'
// Material UI
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'
import Typography from '@material-ui/core/Typography'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Fab from '@material-ui/core/Fab'
import Divider from '@material-ui/core/Divider'
import MenuItem from '@material-ui/core/MenuItem'
import Switch from '@material-ui/core/Switch'
import { MobileStepper, Stepper, Step, StepLabel } from '@material-ui/core'
// @material-ui/icons
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft'
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight'
// useful tools
import isEmpty from '../../../../helpers/is-empty'
// styles
import { MuiThemeProvider } from '@material-ui/core/styles'
// Addition Components
import LinkAnswer from './LinkAnswer'
import CriteriaAnswer from './CriteriaAnswer'
import AssessmentAnswer from './AssessmentAnswer'

const Run = ({
  survey,
  stage,
  questions,
  question,
  answer,
  respondent,
  onChange,
  onClick,
  onFind,
  error,
  loading,
  theme,
  classes,
  handleNext,
  handleBack,
  handleFinish
}) => {
  return (
    <MuiThemeProvider theme={theme}>
      <Grid container direction='column' className={classes.GridContent}>
        {loading ? (
          <LinearIndeterminate />
        ) : (
          <React.Fragment>
            <Paper className={classes.QuestionPaper}>
              <Typography variant='h6' className={classes.QuestionTitle}>
                {question.title}
              </Typography>
              <Grid
                container
                direction='column'
                wrap='nowrap'
                className={classes.QuestionContent}
              >
                {question.type === '1' && (
                  <LinkAnswer
                    onChange={onChange}
                    question={question}
                    theme={theme}
                  />
                )}
                {/* {answer.isCriterias && (
                  <CriteriaAnswer
                    criterias={question.criterias}
                    answer={answer}
                  />
                )}
                {answer.isMembersInQuestion && (
                  <AssessmentAnswer answer={answer} />
                )} */}
              </Grid>

              {survey.questions && stage === survey.questions.length - 1 && (
                <Grid
                  container
                  justify='center'
                  className={classes.GridFinishButton}
                >
                  <Button
                    variant='contained'
                    color='primary'
                    onClick={handleFinish}
                  >
                    Завершить опрос
                  </Button>
                </Grid>
              )}
            </Paper>

            <Grid className={classes.GridStepper}>
              <MobileStepper
                variant='progress'
                steps={questions.length}
                position='static'
                activeStep={stage}
                className={classes.mobileStepper}
                classes={{
                  root: classes.StepperRoot
                }}
                nextButton={
                  <Button
                    size='small'
                    onClick={handleNext}
                    disabled={stage === questions.length - 1}
                  >
                    След.
                    <KeyboardArrowRight />
                  </Button>
                }
                backButton={
                  <Button
                    size='small'
                    onClick={handleBack}
                    disabled={stage === 0}
                  >
                    <KeyboardArrowLeft />
                    Пред.
                  </Button>
                }
              />
            </Grid>
          </React.Fragment>
        )}
      </Grid>
    </MuiThemeProvider>
  )
}

Run.defaultProps = {
  questions: []
}

export default Run
