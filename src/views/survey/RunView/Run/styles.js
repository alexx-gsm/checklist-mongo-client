export default theme => ({
  StepperRoot: {
    maxWidth: '1024px',
    margin: '0 auto'
  },
  GridContent: {
    height: '100%',
    paddingBottom: '50px'
  },
  Container: {
    [theme.breakpoints.down('xs')]: {
      justifyContent: 'center',
      padding: '0 5px'
    }
  },
  FioTypo: {
    paddingBottom: '20px',
    marginTop: '-30px'
  },
  QuestionPaper: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
    marginBottom: '3px'
  },
  QuestionContent: {
    overflow: 'auto',
    flex: 1
  },
  QuestionTitle: {
    padding: '12px 17px',
    lineHeight: 1.2,
    color: theme.palette.common.white,
    background: theme.palette.grey[600],
    [theme.breakpoints.down('sm')]: {
      fontSize: '1rem'
    }
  },
  GridFinishButton: {
    marginTop: 'auto',
    marginBottom: '20px'
  },

  GridStepper: {
    position: 'fixed',
    left: 0,
    bottom: 0,
    right: 0,
    boxShadow:
      '0px 2px 4px -1px rgba(0, 0, 0, 0.2), 0px 4px 5px 0px rgba(0, 0, 0, 0.14), 0px 1px 10px 0px rgba(0, 0, 0, 0.12)'
  }
})
