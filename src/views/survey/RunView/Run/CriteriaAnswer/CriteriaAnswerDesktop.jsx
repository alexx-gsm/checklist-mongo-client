import React from 'react'
import classNames from 'classnames'
// @material-ui components
import {
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell
} from '@material-ui/core'
import { Typography, TextField, Grid, MenuItem } from '@material-ui/core'
// useful tools
import isEmpty from '../../../../../helpers/is-empty'

const CriteriaAnswerDesktop = ({
  result,
  criterias,
  answer,
  onChange,
  classes
}) => (
  <Table className={classNames(classes.Table, classes.extraTable)}>
    <TableBody>
      {criterias &&
        Object.keys(criterias).map((itemId, index) => {
          return (
            <TableRow key={index} className={classes.TableBodyRow}>
              <TableCell padding='checkbox'>
                <Typography variant='h6' className={classes.typoAutoInput}>
                  {criterias[itemId].criterion}
                </Typography>
              </TableCell>
              <TableCell width='25%' padding='checkbox'>
                {answer.variants && (
                  <TextField
                    select
                    fullWidth
                    id='answerId'
                    value={isEmpty(result[index]) ? '' : result[index]}
                    onChange={onChange(index)}
                    className={classes.wrapTitle}
                    InputProps={{
                      className: classes.Title
                    }}
                  >
                    {answer.variants.map((item, index) => {
                      return (
                        <MenuItem key={index} value={index}>
                          <Grid
                            container
                            alignItems='baseline'
                            className={classes.gridType}
                            direction='row'
                            wrap='wrap'
                          >
                            <Typography
                              className={classes.typoSelectInputTitle}
                              variant='h6'
                            >
                              {item.title}
                            </Typography>
                          </Grid>
                        </MenuItem>
                      )
                    })}
                  </TextField>
                )}
              </TableCell>
            </TableRow>
          )
        })}
    </TableBody>
  </Table>
)

export default CriteriaAnswerDesktop
