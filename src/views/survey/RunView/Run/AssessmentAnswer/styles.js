export default theme => ({
  extraTable: {
    marginBottom: '20px'
  },
  typoAutoInput: {
    lineHeight: '1.2',
    padding: '8px 5px',
    [theme.breakpoints.down('sm')]: {
      fontSize: '1.1rem',
      lineHeight: 1.1
    },
    [theme.breakpoints.down('xs')]: {
      padding: '8px 0',
      fontSize: '1rem',
      fontWeight: theme.typography.fontWeightMedium,
      color: theme.palette.extraCardColor[800]
    }
  },
  typoSelectInputTitle: {
    lineHeight: 1,
    [theme.breakpoints.down('sm')]: {
      fontSize: '1.2rem'
    }
  },
  GridActionButton: {
    padding: '20px 8px',
    [theme.breakpoints.down('sm')]: {
      justifyContent: 'center'
    }
  },
  GridButtonAdd: {
    padding: '20px',
    paddingLeft: '8px',
    [theme.breakpoints.down('sm')]: {
      justifyContent: 'center'
    }
  }
})
