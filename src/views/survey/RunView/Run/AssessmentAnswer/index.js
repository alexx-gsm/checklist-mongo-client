import { compose, withState, withProps, withHandlers } from 'recompose'
import { connect } from 'react-redux'
// AC
import { storeStageResult } from '../../../../../redux/modules/survey'
// Hocs
import withDialog from '../../../../../hocs/withDialog'
// Styles
import { withStyles } from '@material-ui/core/styles'
import styles from './styles'
import CardStyles from '../../../../../styles/CardStyles'
import CommonStyles from '../../../../../styles/CommonStyles'
import CardTableStyles from '../../../../../styles/CardTableStyles'

import AssessmentAnswer from './AssessmentAnswer'

export default compose(
  connect(
    ({ respondentStore, surveyStore }) => {
      const { respondents } = respondentStore
      const { result, prevResult } = surveyStore

      return {
        respondents,
        result,
        prevResult
      }
    },
    { storeStageResult }
  ),
  withState('isAuto', 'setIsAuto', false),
  withState('isVisible', 'setVisible', false),
  withState('index', 'setIndex', null),
  withState('user', 'setUser', null),
  withProps({
    modalTitle: 'Удалить?',
    modalText: 'Вы уверены, что хотите удалить эту строку?'
  }),
  withHandlers({
    handleAutocomplete: ({ result, storeStageResult, setIsAuto }) => item => {
      storeStageResult([...result, { item, variant: '' }])
      setIsAuto(false)
    },
    onAddRow: ({ setIsAuto }) => () => setIsAuto(true),
    onDeleteRow: ({ result, setIndex, setVisible, setUser }) => index => () => {
      setIndex(index)
      setUser(result[index].item.fio)
      setVisible(true)
    },
    onChange: ({ result, storeStageResult }) => index => event => {
      result[index] = {
        item: result[index].item,
        variant: event.target.value
      }

      storeStageResult(result)
    },
    handlePrevDonload: ({ prevResult, storeStageResult }) => () => {
      const loadedResult = prevResult.map(result => {
        return {
          item: result.item,
          variant: null
        }
      })

      storeStageResult(loadedResult)
    }
  }),

  withStyles(
    theme => ({
      ...CardStyles(theme),
      ...CommonStyles(theme),
      ...CardTableStyles(theme),
      ...styles(theme)
    }),
    {
      theme: true
    }
  ),
  withDialog
)(AssessmentAnswer)
