import React from 'react'
import classNames from 'classnames'
import Autocomplete from '../../../../../components/Autocomplete'
// @material-ui components
import {
  Table,
  TableBody,
  TableRow,
  TableCell,
  Button
} from '@material-ui/core'
import { Paper } from '@material-ui/core'
import { Typography, TextField, Grid, MenuItem } from '@material-ui/core'
// useful tools
import isEmpty from '../../../../../helpers/is-empty'
// @material-ui/icons
import AddCircle from '@material-ui/icons/AddCircle'
import RemoveCircleOutline from '@material-ui/icons/RemoveCircleOutline'

const AssessmentAnswer = ({
  isAuto,
  result,
  prevResult,
  answer,
  onAddRow,
  onDeleteRow,
  onChange,
  respondents,
  handleAutocomplete,
  handlePrevDonload,
  classes
}) => {
  return (
    <React.Fragment>
      {!isEmpty(prevResult) && (
        <Grid container className={classes.GridActionButton}>
          <Button
            variant='contained'
            color='secondary'
            onClick={handlePrevDonload}
          >
            Загрузить варианты с пред вопроса
          </Button>
        </Grid>
      )}

      <Table className={classNames(classes.Table, classes.extraTable)}>
        <TableBody>
          {result &&
            result.map(({ item, variant }, index) => {
              return (
                <TableRow key={item.code} className={classes.TableBodyRow}>
                  <TableCell
                    className={classNames(classes.removeCell, 'icon-remove')}
                  >
                    <RemoveCircleOutline
                      className={classes.removeIcon}
                      onClick={onDeleteRow(index)}
                    />
                  </TableCell>
                  <TableCell className={classes.TableCellTitle}>
                    <Typography variant='h6' className={classes.typoAutoInput}>
                      {item.fio}
                    </Typography>
                    {answer.variants && (
                      <TextField
                        select
                        fullWidth
                        id='answerId'
                        value={isEmpty(variant) ? '' : variant}
                        onChange={onChange(index)}
                        margin='none'
                        className={classes.wrapTitle}
                        InputProps={{
                          className: classes.Title
                        }}
                      >
                        {answer.variants.map((item, index) => {
                          return (
                            <MenuItem key={index} value={index}>
                              <Grid
                                container
                                alignItems='baseline'
                                className={classes.gridType}
                                direction='row'
                                wrap='wrap'
                              >
                                <Typography
                                  className={classes.typoSelectInputTitle}
                                  variant='h6'
                                >
                                  {item.title}
                                </Typography>
                              </Grid>
                            </MenuItem>
                          )
                        })}
                      </TextField>
                    )}
                  </TableCell>
                </TableRow>
              )
            })}
        </TableBody>
      </Table>
      {isAuto ? (
        <Paper square className={classes.PaperAuto}>
          <Autocomplete
            items={respondents}
            field='fio'
            onChange={handleAutocomplete}
            placeholder='ФИО сотрудника'
          />
        </Paper>
      ) : (
        <Grid container justify='center' className={classes.MB20}>
          <AddCircle className={classes.addIcon} onClick={onAddRow} />
        </Grid>
      )}
    </React.Fragment>
  )
}

AssessmentAnswer.defaultProps = {
  respondents: {}
}

export default AssessmentAnswer
