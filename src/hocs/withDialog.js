import React from 'react'
// Material UI
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import { withStyles } from '@material-ui/core/styles'

const AlertDialog = withStyles(theme => ({
  Dialog: {
    minWidth: '400px',
    [theme.breakpoints.down('xs')]: {
      minWidth: '330px'
    }
  },
  Title: {
    borderBottom: `1px solid ${theme.palette.divider}`,
    margin: 0,
    padding: theme.spacing.unit * 2
  },
  Content: {
    padding: `${theme.spacing.unit * 3}px ${theme.spacing.unit * 2}px`
  },
  Actions: {
    borderTop: `1px solid ${theme.palette.divider}`,
    margin: 0,
    padding: theme.spacing.unit * 2
  }
}))(({ title, text, user, isVisible, onCancel, onConfirm, classes }) => {
  return (
    <div>
      <Dialog
        open={isVisible}
        aria-labelledby='alert-dialog-title'
        aria-describedby='alert-dialog-description'
        classes={{
          paper: classes.Dialog
        }}
      >
        <DialogTitle id='alert-dialog-title' className={classes.Title}>
          {title}
        </DialogTitle>
        <DialogContent className={classes.Content}>
          <DialogContentText id='alert-dialog-description'>
            {user}
          </DialogContentText>
        </DialogContent>
        <DialogActions className={classes.Actions}>
          <Button
            color='secondary'
            size='small'
            onClick={() => {
              onConfirm()
              onCancel()
            }}
          >
            Удалить
          </Button>
          <Button variant='contained' onClick={onCancel} autoFocus>
            Отмена
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
})

const withDialog = Component => ({
  modalTitle,
  modalText,
  isVisible,
  setVisible,
  index,
  user,
  ...props
}) => (
  <div>
    <Component {...props} />
    <AlertDialog
      title={modalTitle}
      text={modalText}
      user={user}
      isVisible={isVisible}
      onCancel={() => setVisible(false)}
      onConfirm={() =>
        props.storeStageResult([
          ...props.result.slice(0, index),
          ...props.result.slice(index + 1)
        ])
      }
    />
  </div>
)

export default withDialog
