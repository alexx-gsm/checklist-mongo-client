import { compose, withHandlers } from 'recompose';
import { connect } from 'react-redux';
import { toggleSidebar } from '../redux/modules/sidebar';

const withSidebar = Component =>
  compose(
    connect(
      ({ sidebar }) => ({ isOpen: sidebar.isOpen }),
      { toggleSidebar }
    ),
    withHandlers({
      onSidebarToggle: ({ toggleSidebar }) => toggleSidebar
    })
  )(Component);

export default withSidebar;
